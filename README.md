2018 Computer Graphics homeworks and project

Final Project  : Spaceship(X-wing) simulator

* Compile: make
* Run: ./xsim

Mode change : c
* Game mode : The spaceship can dodge asteroids or
        shoot laser beams and destroy asteroids to avoid the crash.
* Cockpit mode : inside X-wing cockpit
* X-wing mode: Showing X-wing

 Key bindings:
    c/C        Toggle game mode/cockpit mode/X-wing mode
    q/Q        Toggle wing shape
     w/W        Xwing move up
     s/S        Xwing move down
     a/A        Xwing move left
     d/D        Xwing move right
     z/Z        On/Off laser beam
     r/R        X-wing restart
     j/J        Toggle asteroid random mode
     g/G        Speed down
     h/G        Speed up
     p/P        Toggle projection mode
     l          Toggles lighting
     m          Toggles light movement
     t/T        Decrease/increase ambient light
     y/Y        Decrease/increase diffuse light
     u/U        Decrease/increase specular light
     i/I        Decrease/increase emitted light
     o/O        Decrease/increase shininess
     F1         Toggle smooth/flat shading
     F2         Toggle local viewer mode
     F3         Toggle light distance (1/5)
     F8         Change ball increment
     F9         Invert bottom normal
     []         Lower/rise light
     p          Toggles ortogonal/perspective projection
     +/-        Change field of view of perspective
     x          Toggle axes
     arrows     Change view angle
     PgDn/PgUp  Zoom in and out
     0          Reset view angle
     ESC        Exit
      Arrows    Change view angle
             Right: Increase azimuth by 5 degrees
             Left : Decrease azimuth by 5 degrees
             Up   : Increase elevation by 5 degrees
      Down : Decrease elevation by 5 degrees

I modified my hw6.c and ex13.c ,ex14.c, ex18.c, ex20.c and ex25.c exmaples provided by Prof. Schreuder for this course

* Finished works

    -  Draw objects: Space area, X-Wing, asteroids
    -  Control X-Wing for fly
    -  Cockpit mode for X-Wing and X-wing showing mode
    -  Laser beam Shoot
    -  Randomly appeared asteroids
    -  Collision function for asteroid lazer,
    -  Effect for collision
    -  Speed control