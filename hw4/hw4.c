/*
 * Homework 4 : Projections
 * Prof. Willem A. (Vlakkies) Schreuder
 * Course: CSCI4229/5229 Fall 2018
 * Name: Jaeyoung Oh
 * SID : 108601183
 *
 *  Key bindings:
 *   m/M       Toggle between perspective and orthogonal
 * +/-       Changes field of view for perspective
 * l/L       Toggle axes
 * PgDn/PgUp Zoom in and out
 * Arrows    Change view angle
 *	    Right: Increase azimuth by 5 degrees
 *	    Left : Decrease azimuth by 5 degrees
 *	    Up   : Increase elevation by 5 degrees
 *	    Down : Decrease elevation by 5 degrees
 * 0          Reset view angle
 * o/O        Camera On/Off 
 * w/W        Camera moves forward
 * s/S        Camera moves backward
 * e/E        Camera moves upward
 * q/Q        Camera moves downward
 * a/A        Camera looks Left
 * d/D        Camera looks Right
 * x/X        Reset Camera Position
 * ESC    Exit
 * I modified my hw3.c and ex9c exmaple provided by Prof. Schreuder for this course.
 * 
 * Time: total 7 hours
 * - web search about the first person view and code review: 5H
 * - HW4.c coding: 2H
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
//  OpenGL with prototypes for glext
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

int axes=0;       //  Display axes
int mode=0;       //  Projection mode
int th=0;         //  Azimuth of view angle
int ph=0;         //  Elevation of view angle
int fov=55;       //  Field of view (for perspective)
double asp=1;     //  Aspect ratio
double dim=5.0;   //  Size of world

// First person 
int Co = 0;        //  Camera On/Off flag
int Cr = 0;        //  Camera angle

//Eye Position
double Ex = 0;
double Ey = 0.5;
double Ez = 15;

//Look at Position
double Cx = 0;
double Cy = 0;
double Cz = 0;

//  Macro for sin & cos in degrees
#define Cos(th) cos(3.1415926/180*(th))
#define Sin(th) sin(3.1415926/180*(th))

/*
 *  Convenience routine to output raster text
 *  Use VARARGS to make this more flexible
 */
#define LEN 8192  //  Maximum length of text string
void Print(const char* format , ...)
{
   char    buf[LEN];
   char*   ch=buf;
   va_list args;
   //  Turn the parameters into a character string
   va_start(args,format);
   vsnprintf(buf,LEN,format,args);
   va_end(args);
   //  Display the characters one at a time at the current raster position
   while (*ch)
      glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,*ch++);
}

/*
 *  Set projection
 */
static void Project()
{
   //  Tell OpenGL we want to manipulate the projection matrix
   glMatrixMode(GL_PROJECTION);
   //  Undo previous transformations
   glLoadIdentity();
   //  First person 
   if(Co){
       gluPerspective(fov,asp,dim/4,4*dim);
   }
   else{
   //  Perspective transformation
        if (mode){
            gluPerspective(fov,asp,dim/4,4*dim);
        }
   //  Orthogonal projection
        else{
            glOrtho(-asp*dim,+asp*dim, -dim,+dim, -dim,+dim);
        }
   }
   //  Switch to manipulating the model matrix
   glMatrixMode(GL_MODELVIEW);
   //  Undo previous transformations
   glLoadIdentity();
}

/*
 *  Draw a cube
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 */
static void cube(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{
   //  Save transformation
   glPushMatrix();
   //  Offset
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(dx,dy,dz);
   //  Cube
   glBegin(GL_QUADS);
   //  Front
   //glColor3f(1,0,0);
   glColor3f(Sin(th)*Sin(th) , Sin(th)*Sin(th) , Cos(th)*Cos(th));
   glVertex3f(-1,-1, 1);
   glVertex3f(+1,-1, 1);
   glVertex3f(+1,+1, 1);
   glVertex3f(-1,+1, 1);
   //  Back
   //glColor3f(0,0,1);
   glColor3f(Cos(th)*Cos(th) , Sin(th)*Sin(th) , Sin(th)*Sin(th));
   glVertex3f(+1,-1,-1);
   glVertex3f(-1,-1,-1);
   glVertex3f(-1,+1,-1);
   glVertex3f(+1,+1,-1);
   //  Right
   //glColor3f(1,1,0);
   glColor3f(Sin(th)*Cos(th) , Sin(th)*Sin(th) , Sin(th)*Sin(th));
   glVertex3f(+1,-1,+1);
   glVertex3f(+1,-1,-1);
   glVertex3f(+1,+1,-1);
   glVertex3f(+1,+1,+1);
   //  Left
   //glColor3f(0,1,0);
   glColor3f(Cos(th)*Cos(th) , Cos(th)*Cos(th) , Sin(th)*Sin(th));
   glVertex3f(-1,-1,-1);
   glVertex3f(-1,-1,+1);
   glVertex3f(-1,+1,+1);
   glVertex3f(-1,+1,-1);
   //  Top
   glColor3f(0,1,1);
   glVertex3f(-1,+1,+1);
   glVertex3f(+1,+1,+1);
   glVertex3f(+1,+1,-1);
   glVertex3f(-1,+1,-1);
   //  Bottom
   glColor3f(1,0,1);
   glVertex3f(-1,-1,-1);
   glVertex3f(+1,-1,-1);
   glVertex3f(+1,-1,+1);
   glVertex3f(-1,-1,+1);
   //  End
   glEnd();
   //  Undo transofrmations
   glPopMatrix();
}

/*
 *  Draw a cone
 *     at (x,y,z)
 *     size (radius, height)
 *     rotated th about the y axis
 * reference site: https://stackoverflow.com/questions/19245363/opengl-glut-surface-normals-of-cone 
 */
static void cone(double x,double y,double z,
                 double radius,double height,
                 double th)
{
   const int d = 5;
   //  Save transformation
   glPushMatrix();
   //  Offset
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(radius,height,radius);

   // draw the upper part of the cone
   glBegin(GL_TRIANGLE_FAN);
   glVertex3f(0, height, 0);
   for (int angle = 0; angle <= 360; angle+=d) {
       glColor3f(Cos(angle)*Cos(angle) , Sin(angle)*Sin(angle) , Sin(angle)*Sin(angle));
       glVertex3f(Sin(angle) * radius, 0, Cos(angle) * radius);
   }
   glEnd();

   // draw the base of the cone
   glColor3f(0,1,0); 
   glBegin(GL_TRIANGLE_FAN);
   glVertex3f(0, 0, 0);
   for (int angle = 0; angle <= 360; angle+=d) {
       // normal is just pointing down
       glColor3f(Cos(angle)*Cos(angle) , Sin(angle)*Sin(angle) , Sin(angle)*Sin(angle));
       glVertex3f(Sin(angle) * radius, 0, Cos(angle) * radius);
   }
   glEnd();

   //  Undo transformations
   glPopMatrix();
}

/*
 *  Draw a cylinder
 *     at (x,y,z)
 *     size (radius, height)
 *     rotated th about the y axis
 * reference site :  https://gist.github.com/davidwparker/1195852
 */
static void cylinder(double x,double y,double z,
                 double radius,double height,
                 double th)
{
   const int d = 5;
   //  Save transformation
   glPushMatrix();
   //  Offset
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(radius,height,radius);

   // draw the upper part of the cylinder
   glBegin(GL_TRIANGLE_FAN);
   glVertex3f(0, height, 0);
   for (int angle = 0; angle <= 360; angle+=d) {
       glColor3f(Cos(angle)*Cos(angle) , Sin(angle)*Sin(angle) , Sin(angle)*Sin(angle));
       glVertex3f(Sin(angle) * radius, height, Cos(angle) * radius);
   }
   glEnd();
   // draw the side part of the cylinder
   glBegin(GL_QUAD_STRIP);
   for (int angle = 0; angle <= 360; angle+=d) {
       glColor3f(Sin(angle)*Sin(angle) , Sin(angle)*Sin(angle) , Cos(angle)*Cos(angle));
       glVertex3f(Sin(angle) * radius, height, Cos(angle) * radius);
       glVertex3f(Sin(angle) * radius, 0, Cos(angle) * radius);
   }
   glEnd();
   // draw the base part of the cylinder
   glBegin(GL_TRIANGLE_FAN);
   glVertex3f(0, 0, 0);
   for (int angle = 0; angle <= 360; angle+=d) {
       glColor3f(Cos(angle)*Cos(angle) , Sin(angle)*Sin(angle) , Sin(angle)*Sin(angle));
       glVertex3f(Sin(angle) * radius, 0, Cos(angle) * radius);
   }
   glEnd();

   //  Undo transformations
   glPopMatrix();
}
/*
 *  OpenGL (GLUT) calls this routine to display the scene
 */
void display()
{
   int i,j;
   const double len=1.5;  //  Length of axes
   //  Erase the window and the depth buffer
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
   //  Enable Z-buffering in OpenGL
   glEnable(GL_DEPTH_TEST);
   //  Undo previous transformations
   glLoadIdentity();
   //First person camera
   if(Co)
   {
        Cx = Sin(Cr);
        Cz = -Cos(Cr);
        Cy = 1;

        gluLookAt(Ex,Ey,Ez, Cx+Ex,Ey,Cz+Ez, 0,1,0);
   }
   //  Perspective - set eye position
   else{
       if (mode)
        {
            double Ex = -2*dim*Sin(th)*Cos(ph);
            double Ey = +2*dim        *Sin(ph);
            double Ez = +2*dim*Cos(th)*Cos(ph);
            gluLookAt(Ex,Ey,Ez , 0,0,0 , 0,Cos(ph),0);
        }
        //  Orthogonal - set world orientation
        else
        {
            glRotatef(ph,1,0,0);
            glRotatef(th,0,1,0);
        }
   }
   // The ground
   glBegin(GL_QUADS);
   glColor3f(0.0,0.5,0.0);
   glVertex3f(5,0,5);
   glVertex3f(5,0,-5);
   glVertex3f(-5,0,-5);
   glVertex3f(-5,0,5);
   glEnd();
   // Draw cubes
   // building with cone
   for (j=1;j<= 3;j++){
       cube(0,j,0,1,1,1,0);
   }
   cone(0,4,0,1.0,1,0);
   // building 
   for(i=-2;i<4;i=i+2){
        cube(i,1,-3 ,0.5,1,0.5,45+i);
        cube(i,2,-3 ,0.3,1,0.3,90+i);
        cube(i,3,-3 ,0.2,1,0.2,135+i);
   }
   //Draw Tree 
   for(i=-3;i<4;i=i+3){
        cone(-4,1,i,1,1,0);
        cylinder(-4,0,i,0.4,1,0);
   }
   //Draw Tree wilth multiple cones  
   for(i=0;i<=6;i=i+2){ 
        cone(4,1,i-3,1,0.7,0);
        cone(4,1.5,i-3,0.8,0.6,25);
        cone(4,1.7,i-3,0.7,0.5,25);
        cone(4,2,i-3,0.6,0.5,35);
        cone(4,2.2,i-3,0.5,0.5,35);
        cone(4,2.5,i-3,0.4,0.5,45);
        cylinder(4,0,i-3,0.3,1.6,60);
   }
   // Draw tree 
   for(i = 0; i<5;i++){
        cone(-2.5,0.5,3-i,0.7,0.8,0);
        cylinder(-2.5,0,3-i,0.3,0.8,0);
   }
   //  Draw axes
   glColor3f(1,1,1);
   if (axes)
   {
      glBegin(GL_LINES);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(len,0.0,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,len,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,0.0,len);
      glEnd();
      //  Label axes
      glRasterPos3d(len,0.0,0.0);
      Print("X");
      glRasterPos3d(0.0,len,0.0);
      Print("Y");
      glRasterPos3d(0.0,0.0,len);
      Print("Z");
   }
   //  Display parameters
   glWindowPos2i(5,5);
   if(Co)
   {
       Print("First person: On, Camera position =%.1f,%.1f,%.1f, Angle=%d",Ex,Ey,Ez,Cr);
   }
   else
   {
   Print("Angle=%d,%d  Dim=%.1f FOV=%d Projection=%s",th,ph,dim,fov,mode?"Perpective":"Orthogonal");
   }
   //  Render the scene and make it visible
   glFlush();
   glutSwapBuffers();
}

/*
 *  GLUT calls this routine when an arrow key is pressed
 */
void special(int key,int x,int y)
{
   //  Right arrow key - increase angle by 5 degrees
   if (key == GLUT_KEY_RIGHT)
      th += 5;
   //  Left arrow key - decrease angle by 5 degrees
   else if (key == GLUT_KEY_LEFT)
      th -= 5;
   //  Up arrow key - increase elevation by 5 degrees
   else if (key == GLUT_KEY_UP)
      ph += 5;
   //  Down arrow key - decrease elevation by 5 degrees
   else if (key == GLUT_KEY_DOWN)
      ph -= 5;
   //  PageUp key - increase dim
   else if (key == GLUT_KEY_PAGE_UP)
      dim += 0.1;
   //  PageDown key - decrease dim
   else if (key == GLUT_KEY_PAGE_DOWN && dim>1)
      dim -= 0.1;
   //  Keep angles to +/-360 degrees
   th %= 360;
   ph %= 360;
   //  Update projection
   Project();
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when a key is pressed
 */
void key(unsigned char ch,int x,int y)
{
   //  Exit on ESC
   if (ch == 27)
      exit(0);
   //  Reset view angle
   else if (ch == '0')
      th = ph = 0;
   //  Toggle axes
   else if (ch == 'l' || ch == 'L')
      axes = 1-axes;
   //  Switch display mode
   else if (ch == 'm' || ch == 'M')
      mode = 1-mode;
   //  Change field of view angle
   else if (ch == '-' && ch>1)
      fov--;
   else if (ch == '+' && ch<179)
      fov++;

   //  Camera On/Off
   else if (ch == 'o' || ch == 'O')
   {
     Co = 1-Co;
   }
   //  First person camera is On
   else if (Co)
   {
     double n = 0.1;
     //  Reset Camera Position
     if (ch == 'x' || ch == 'X')
     {
        Ex = Cx = Cz = Cr = 0;
	    Ey = 0.5;
        Ez = 15;
     }
     //First person camera moves forward
     else if (ch == 'w' || ch == 'W')
     {
       Ex += n*Cx;
       Ez += n*Cz;
     }
     //Camera moves backward
     else if (ch == 's' || ch == 'S')
     {
       Ex -= n*Cx;
       Ez -= n*Cz;
     }
     //Camera moves upward
     else if (ch == 'q' || ch == 'Q')
     {
       Ey += n*Cy;
     }
     //Camera moves downward
     else if (ch == 'e' || ch == 'E')
     {
       Ey -= n*Cy;
     }
     //Camera looks left
     else if (ch == 'a' || ch == 'A')
       Cr -= 1;
     //Camera looks right
     else if (ch == 'd' || ch == 'D')
       Cr += 1;
     //  Keep angles to +/-360 degrees
       Cr %= 360;
   }
   //  Reproject
   Project();
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when the window is resized
 */
void reshape(int width,int height)
{
   //  Ratio of the width to the height of the window
   asp = (height>0) ? (double)width/height : 1;
   //  Set the viewport to the entire window
   glViewport(0,0, width,height);
   //  Set projection
   Project();
}

/*
 *  Start up GLUT and tell it what to do
 */
int main(int argc,char* argv[])
{
   //  Initialize GLUT
   glutInit(&argc,argv);
   //  Request double buffered, true color window with Z buffering at 800x600
   glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
   glutInitWindowSize(900,600);
   glutCreateWindow("HW4: Jaeyoung Oh");
   //  Set callbacks
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutSpecialFunc(special);
   glutKeyboardFunc(key);
   //  Pass control to GLUT so it can interact with the user
   glutMainLoop();
   return 0;
}
