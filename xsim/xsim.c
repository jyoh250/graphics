/*
 * Project review : spaceship(xwing) simulator
 * Prof. Willem A. (Vlakkies) Schreuder
 * Course: CSCI 5229 Fall 2018
 * Name: Jaeyoung Oh
 * SID : 108601183
 *
 * Key bindings:
 * c/C        Toggle game mode/cockpit mode/X-wing mode
 * q/Q        Toggle wing shape
 * w/W        Xwing move up
 * s/S        Xwing move down
 * a/A        Xwing move left
 * d/D        Xwing move right
 * z/Z        On/Off laser beam
 * r/R        X-wing restart
 * j/J        Toggle asteroid random mode
 * g/G        Speed down
 * h/G        Speed up
 * p/P        Toggle projection mode
 * l          Toggles lighting
 * m          Toggles light movement
 * t/T        Decrease/increase ambient light
 * y/Y        Decrease/increase diffuse light
 * u/U        Decrease/increase specular light
 * i/I        Decrease/increase emitted light
 * o/O        Decrease/increase shininess
 * F1         Toggle smooth/flat shading
 * F2         Toggle local viewer mode
 * F3         Toggle light distance (1/5)
 * F8         Change ball increment
 * F9         Invert bottom normal
 * []         Lower/rise light
 * p          Toggles ortogonal/perspective projection
 * +/-        Change field of view of perspective
 * x          Toggle axes
 * arrows     Change view angle
 * PgDn/PgUp  Zoom in and out
 * 0          Reset view angle
 * ESC        Exit
 * Arrows    Change view angle
 *	 Right: Increase azimuth by 5 degrees
 *	 Left : Decrease azimuth by 5 degrees
 *	 Up   : Increase elevation by 5 degrees
 * Down : Decrease elevation by 5 degrees
 *  
 * Time: total 70 hours
 * - web search about the moving and key binding code review: 20H
 * - xsim.c coding: 50H
 */


#include "CSCIx229.h"
#define PI 3.14159265358979323846

int axes=0;       //  Display axes
int mode=1;       //  Projection mode
int move=1;       //  Movie light
int th=0;         //  Azimuth of view angle
int ph=0;         //  Elevation of view angle
int fov=55;       //  Field of view (for perspective)
double asp=1;     //  Aspect ratio
double dim=5.0;   //  Size of world
int wingMode = 1; // wing shift

// Light values
int light     =   1;  //lighting
int one       =   1;  // Unit value
int distance  =   5;  // Light distance
int inc       =  10;  // Ball increment
int smooth    =   1;  // Smooth/Flat shading
int local     =   0;  // Local Viewer Model
int emission  =   0;  // Emission intensity (%)
int ambient   =  30;  // Ambient intensity (%)
int diffuse   = 100;  // Diffuse intensity (%)
int specular  =   0;  // Specular intensity (%)
int shininess =   0;  // Shininess (power of two)
float shiny   =   1;  // Shininess (value)
int zh        =  90;  // Light azimuth
float ylight  =   0;  // Elevation of light
unsigned int texture[16];  //  Texture names
int ntx = 0;           // Texture number
float rep = 0.0;      // cube texture repeat number
double Ex = 1;   //  Eye
double Ey = 1;   //  Eye
double Ez = 1;   //  Eye
//Look at Position
double Cx = 0;
double Cy = 0;
double Cz = 0;
static double mx = 0.0;   //xwing position
static double my = 0.0;
static double px1 = 0.0;   //asteroid position
static double py1 = 1.0;
static double pr1 = 0.3;
static double px2 = 1.0;   //asteroid position
static double py2 = 2.0;
static double pr2 = 0.4;
static double thrust = 0.5; // speed 
int plive1 = 1;
int plive2 = 1;
int boolShoot = 0;
int randomMode = 0;  //random appear
double zOffset = 0.0; //space scroll
double bOffset = 0.0; //shoot scroll
int explodeMode = 0; //explosion
int PexplodeMode = 0; //explosion
int cockpitMode = 0; //explosion
int Cr = 0;        //  Camera angle
double wx[2] = {-1.4,1.4};
double wy[2] = {-0.4,0.4};
float explosionScale=1.0f;
float delta = 1.0f;
int alive = 1;
int appY = 600;

/*
 *  Draw vertex in polar coordinates with normal from ex13.c add texture coordinate
 */
static void Vertex(double th,double ph)
{
   double x = Sin(th)*Cos(ph);
   double y = Cos(th)*Cos(ph);
   double z =         Sin(ph);
   //  For a sphere at the origin, the position
   //  and normal vectors are the same
   glNormal3d(x,y,z);
   glTexCoord2d(th/360.0,ph/180.0+0.5);
   glVertex3d(x,y,z);
}
/**
 * randBetween
 * Returns a random number between the given min and max.
 */
float randBetween(int min, int max) {
	if (max <= min) {
		return min;
	}
	return ((rand() % (max - min)) + min);
}
/*
 *  Draw a cube from ex13.c and add texture functioin
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 *     rep: y repeat number of texture
 */
static void cube(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th, float rep)
{
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(dx,dy,dz);
   // Texture 
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[4]);
   glColor3f(1,1,1);
   //  Cube
   glBegin(GL_QUADS);
   //  Front
   //glColor3f(1,1,1);
   glNormal3f( 0, 0, 1);
   glTexCoord2f(0.0,rep); glVertex3f(-1,-1, 1);
   glTexCoord2f(1.0,rep); glVertex3f(+1,-1, 1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,+1, 1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,+1, 1);
   //  Back
   //glColor3f(1,1,1);
   glNormal3f( 0, 0,-1);
   glTexCoord2f(0.0,rep); glVertex3f(+1,-1,-1);
   glTexCoord2f(1.0,rep); glVertex3f(-1,-1,-1);
   glTexCoord2f(1.0,0.0); glVertex3f(-1,+1,-1);
   glTexCoord2f(0.0,0.0); glVertex3f(+1,+1,-1);
      //  Right
   //glColor3f(1,1,1);
   glNormal3f(+1, 0, 0);
   glTexCoord2f(0.0,rep); glVertex3f(+1,-1,+1);
   glTexCoord2f(1.0,rep); glVertex3f(+1,-1,-1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,+1,-1);
   glTexCoord2f(0.0,0.0); glVertex3f(+1,+1,+1);
      //  Left
   //glColor3f(1,1,1);
   glNormal3f(-1, 0, 0);
   glTexCoord2f(0.0,rep); glVertex3f(-1,-1,-1);
   glTexCoord2f(1.0,rep); glVertex3f(-1,-1,+1);
   glTexCoord2f(1.0,0.0); glVertex3f(-1,+1,+1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,+1,-1);
      //  Top
   //glColor3f(1,1,1);
   glNormal3f( 0,+1, 0);
   glTexCoord2f(0.0,rep); glVertex3f(-1,+1,+1);
   glTexCoord2f(1.0,rep); glVertex3f(+1,+1,+1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,+1,-1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,+1,-1);
      //  Bottom
   //glColor3f(1,1,1);
   glNormal3f( 0,-1, 0);
   glTexCoord2f(0.0,rep); glVertex3f(-1,-1,-1);
   glTexCoord2f(1.0,rep); glVertex3f(+1,-1,-1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,-1,+1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,-1,+1);
     //  End
   glEnd();
   //  Undo transofrmations
    //  Switch off textures so it doesn't apply to the rest
   glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw a cube from ex13.c and add texture functioin
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 *     rep: y repeat number of texture
 */
static void cubeN(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{

   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(dx,dy,dz);
    
   // Texture 
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[7]);
   glColor3f(1,1,1);
   //  Cube
   glBegin(GL_QUADS);
   //  Front
   //glColor3f(1,1,1);
   glNormal3f( 0, 0, 1);
   glTexCoord2f(0.0,1.0); glVertex3f(-1,-1, 1);
   glTexCoord2f(1.0,1.0); glVertex3f(+1,-1, 1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,+1, 1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,+1, 1);
   //  Back
   //glColor3f(1,1,1);
   glNormal3f( 0, 0,-1);
   glTexCoord2f(0.0,1.0); glVertex3f(+1,-1,-1);
   glTexCoord2f(1.0,1.0); glVertex3f(-1,-1,-1);
   glTexCoord2f(1.0,0.0); glVertex3f(-1,+1,-1);
   glTexCoord2f(0.0,0.0); glVertex3f(+1,+1,-1);
      //  Right
   //glColor3f(1,1,1);
   glNormal3f(+1, 0, 0);
   glTexCoord2f(0.0,1.0); glVertex3f(+1,-1,+1);
   glTexCoord2f(1.0,1.0); glVertex3f(+1,-1,-1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,+1,-1);
   glTexCoord2f(0.0,0.0); glVertex3f(+1,+1,+1);
      //  Left
   //glColor3f(1,1,1);
   glNormal3f(-1, 0, 0);
   glTexCoord2f(0.0,1.0); glVertex3f(-1,-1,-1);
   glTexCoord2f(1.0,1.0); glVertex3f(-1,-1,+1);
   glTexCoord2f(1.0,0.0); glVertex3f(-1,+1,+1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,+1,-1);
      //  Top
   //glColor3f(1,1,1);
   glNormal3f( 0,+1, 0);
   glTexCoord2f(0.0,1.0); glVertex3f(-1,+1,+1);
   glTexCoord2f(1.0,1.0); glVertex3f(+1,+1,+1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,+1,-1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,+1,-1);
      //  Bottom
   //glColor3f(1,1,1);
   glNormal3f( 0,-1, 0);
   glTexCoord2f(0.0,1.0); glVertex3f(-1,-1,-1);
   glTexCoord2f(1.0,1.0); glVertex3f(+1,-1,-1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,-1,+1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,-1,+1);
     //  End
   glEnd();
   //  Undo transofrmations
    //  Switch off textures so it doesn't apply to the rest
   glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw a cylinder with texture 
 *     at (x,y,z)
 *     size (r(radius), h(height))
 *     rotate (rx, ry, rz)
 */
void cylinder(double x,double y,double z,
              double r,double h, 
              double rx, double ry, double rz,
              unsigned int ctexture)
{
   int inc=5;
   int th;

   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(rx,1,0,0);
   glRotated(ry,0,1,0);
   glRotated(rz,0,0,1);
   glScaled(r,h,r);

   //  Enable textures
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,ctexture);

   glColor3f(1,1,1);
   // Upper Part
   glBegin(GL_TRIANGLE_FAN);
   glTexCoord2f(0.5,0.5); 
   glVertex3f(0,1,0);
   glNormal3f(0,1,0);
   for (th=0;th<=360;th+=2*inc)
   {
      glTexCoord2f(2*Sin(th)+0.5,2*Cos(th)+0.5);
      glVertex3d(Sin(th),1,Cos(th));
   }
   glEnd();

   // Side Part
   glBegin(GL_QUAD_STRIP); 
   for (th=0;th<=360;th+=inc)
   {
      //double u = th/360.0;
      glNormal3f(Cos(th),0,Sin(th));
      // glTexCoord2d(10*u,4.0); glVertex3d(Cos(th),1,Sin(th));
      // glTexCoord2d(10*u,0.0); glVertex3d(Cos(th),0,Sin(th));
      glTexCoord2d(Cos(th),1); glVertex3d(Cos(th),1,Sin(th));
      glTexCoord2d(Cos(th),0); glVertex3d(Cos(th),0,Sin(th));
    }
   glEnd();

   // Base Part
   glBegin(GL_TRIANGLE_FAN);
   glTexCoord2f(0.5,0.5); 
   glVertex3f(0,0,0);
   glNormal3f(0,-1,0);
   for (th=0;th<=360;th+=2*inc)
   {
      glTexCoord2f(2*Sin(th)+0.5,2*Cos(th)+0.5);
      glVertex3d(Sin(th),0,Cos(th));
   }
   glEnd();

   //  Undo transformations and textures
   glPopMatrix();
   glDisable(GL_TEXTURE_2D);
}
/*
 *  Draw a ball from ex13.c
 *     at (x,y,z)
 *     radius (r)
 */
static void ball(double x,double y,double z,double r)
{
   int th,ph;
   float yellow[] = {1.0,1.0,0.0,1.0};
   float Emission[]  = {0.0,0.0,0.01*emission,1.0};
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glScaled(r,r,r);

   //  White ball
   glColor3f(1,1,1);
   glMaterialf(GL_FRONT,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT,GL_SPECULAR,yellow);
   glMaterialfv(GL_FRONT,GL_EMISSION,Emission);
   //  Bands of latitude
   for (ph=-90;ph<90;ph+=inc)
   {
      glBegin(GL_QUAD_STRIP);
      for (th=0;th<=360;th+=2*inc)
      {
         Vertex(th,ph);
         Vertex(th,ph+inc);
      }
      glEnd();
   }
   //  Undo transofrmations
   glPopMatrix();
}
/*
 *  Draw a moon from ex18.c
 *     at (x,y,z)
 *     radius (r)
 */
static void moon(double x,double y,double z,double r,unsigned int mtexture)
{
   int th,ph;
   float yellow[] = {1.0,1.0,0.0,1.0};
   float Emission[]  = {0.0,0.0,0.01*emission,1.0};
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glScaled(r,r,r);
   /*
    *  Draw surface of the planet
    */
   // Set texture
   glEnable(GL_TEXTURE_2D);
   glBindTexture(GL_TEXTURE_2D,mtexture);

   //  White ball
   glColor3f(1,1,1);
   glMaterialf(GL_FRONT,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT,GL_SPECULAR,yellow);
   glMaterialfv(GL_FRONT,GL_EMISSION,Emission);
   //  Bands of latitude
   for (ph=-90;ph<90;ph+=inc)
   {
      glBegin(GL_QUAD_STRIP);
      for (th=0;th<=360;th+=2*inc)
      {
         Vertex(th,ph);
         Vertex(th,ph+inc);
      }
      glEnd();
   }

   //  Undo transofrmations
   glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw xwing nose
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 */
static void nose(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(dx,dy,dz);
    
   glBegin(GL_POLYGON);
   //  Back polygon
   glColor3f(1.0,1.0,1.0);
   glNormal3f(0.0,0.0,-1);
   glVertex3f(-0.13,-0.53,0); //1
   glVertex3f(+0.13,-0.53,0); //2
   glVertex3f(+0.33,0,0);     //3
   glVertex3f(+0.13,+0.13,0); //4
   glVertex3f(-0.13,+0.13,0); //5
   glVertex3f(-0.33,0.0,0);   //6
   glEnd();
   // Front polygon
   glBegin(GL_POLYGON);
   glColor3f(0,0,0);
   glNormal3f(1.0,1.0,1.0);
   glVertex3f(-0.1,-0.1,1); //1
   glVertex3f(+0.1,-0.1,1); //2
   glVertex3f(+0.2,-0.07,1); //3
   glVertex3f(+0.1,-0.05,1); //4
   glVertex3f(-0.1,-0.05,1); //5
   glVertex3f(-0.2,-0.07,1);
   glEnd();
   //Texture 
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[11]);
   glColor3f(1,1,0);
   //up
   glBegin(GL_QUADS);

   glColor3f(1.0,1.0,1.0);
   glNormal3f(0.0,1.0,0.0);
   glTexCoord2d(0,0); glVertex3f(+0.13,+0.13,0);
   glTexCoord2d(1,0); glVertex3f(-0.13,+0.13,0);
   glTexCoord2d(0,1); glVertex3f(-0.1,-0.05,1);
   glTexCoord2d(1,1); glVertex3f(+0.1,-0.05,1);
   
   //side up right
   glNormal3f(0.5,1.0,0.0);
   glTexCoord2d(0,0); glVertex3f(+0.13,+0.13,0);
   glTexCoord2d(1,0); glVertex3f(+0.33,0.0,0);
   glTexCoord2d(0,1); glVertex3f(+0.2,-0.07,1);
   glTexCoord2d(1,1); glVertex3f(+0.1,-0.05,1);
   //side down right
   glNormal3f(0.5,-1.0,0.0);
   glTexCoord2d(0,0); glVertex3f(+0.33,0.0,0);
   glTexCoord2d(1,0); glVertex3f(+0.13,-0.53,0);
   glTexCoord2d(0,1); glVertex3f(+0.1,-0.1,1);
   glTexCoord2d(1,1); glVertex3f(+0.2,-0.07,1);
   //bottom
   glNormal3f(0.0,-1.0,0.0);
   glTexCoord2d(0,0); glVertex3f(-0.13,-0.53,0);
   glTexCoord2d(1,0); glVertex3f(+0.13,-0.53,0);
   glTexCoord2d(0,1); glVertex3f(+0.1,-0.1,1);
   glTexCoord2d(1,1); glVertex3f(-0.1,-0.1,1);
   //side down left
   glNormal3f(-0.5,-1.0,0.0);
   glTexCoord2d(0,0); glVertex3f(-0.33,0.0,0);
   glTexCoord2d(1,0); glVertex3f(-0.13,-0.53,0);
   glTexCoord2d(0,1); glVertex3f(-0.1,-0.1,1);
   glTexCoord2d(1,1); glVertex3f(-0.2,-0.07,1);
   //side up left
   glNormal3f(-0.5,1.0,0.0);
   glTexCoord2d(0,0); glVertex3f(-0.13,0.13,0);
   glTexCoord2d(1,0); glVertex3f(-0.33,0.0,0);
   glTexCoord2d(0,1); glVertex3f(-0.2,-0.07,1);
   glTexCoord2d(1,1); glVertex3f(-0.1,-0.05,1);
   glEnd();
   //  Undo transofrmations
   //  Switch off textures so it doesn't apply to the rest
   //glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw xwing Body front
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 */
static void bodyF(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(dx,dy,dz);
    
   glBegin(GL_POLYGON);
   //  Back polygon
   glColor3f(1,1,1);
   glNormal3f(0.0,0.0,-1);
   glVertex3f(-0.5,-1.0,0);
   glVertex3f(+0.5,-1.0,0);
   glVertex3f(+1.0,0,0);
   glVertex3f(+0.5,+1.0,0);
   glVertex3f(-0.5,+1.0,0);
   glVertex3f(-1.0,0.0,0);
   glEnd();
   // Front polygon
   glBegin(GL_POLYGON);
   glColor3f(1,1,1);
   glNormal3f(0.0,0.0,1);
   glVertex3f(-0.1,-0.5,8);
   glVertex3f(+0.1,-0.5,8);
   glVertex3f(+0.3,0,8);
   glVertex3f(+0.1,+0.1,8);
   glVertex3f(-0.1,+0.1,8);
   glVertex3f(-0.3,0.0,8);
   glEnd();
   
      // Texture 
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[14]);
   glColor3f(1,1,1);
   //up
   glBegin(GL_QUADS);
   glColor3f(1,1,1);
   glNormal3f(0.0,1.0,0.0);
   glTexCoord2d(0,0); glVertex3f(+0.5,+1.0,0);
   glTexCoord2d(1,0); glVertex3f(-0.5,+1.0,0);
   glTexCoord2d(1,1); glVertex3f(-0.1,0.1,8);
   glTexCoord2d(0,1); glVertex3f(+0.1,0.1,8);
   //side up right
   glNormal3f(0.5,1.0,0.0);
   glTexCoord2d(0,0); glVertex3f(+0.5,+1.0,0);
   glTexCoord2d(1,0); glVertex3f(+1.0,0.0,0);
   glTexCoord2d(1,1); glVertex3f(+0.3,0.0,8);
   glTexCoord2d(0,1); glVertex3f(+0.1,+0.1,8);
   //side down right
   glNormal3f(0.5,-1.0,0.0);
   glTexCoord2d(1,0); glVertex3f(+1.0,0.0,0);
   glTexCoord2d(0,0); glVertex3f(+0.5,-1.0,0);
   glTexCoord2d(0,1); glVertex3f(+0.1,-0.5,8);
   glTexCoord2d(1,1); glVertex3f(+0.3,0.0,8);
   //bottom
   glNormal3f(0.0,-1.0,0.0);
   glTexCoord2d(1,0); glVertex3f(-0.5,-1.0,0);
   glTexCoord2d(0,0); glVertex3f(+0.5,-1.0,0);
   glTexCoord2d(0,1); glVertex3f(+0.1,-0.5,8);
   glTexCoord2d(1,1); glVertex3f(-0.1,-0.5,8);
   //side down left
   glNormal3f(-0.5,-1.0,0.0);
   glTexCoord2d(1,0); glVertex3f(-1.0,0.0,0);
   glTexCoord2d(0,0); glVertex3f(-0.5,-1.0,0);
   glTexCoord2d(0,1); glVertex3f(-0.1,-0.5,8);
   glTexCoord2d(1,1); glVertex3f(-0.3,0.0,8);
   //side up left
   glNormal3f(-0.5,1.0,0.0);
   glTexCoord2d(1,0); glVertex3f(-0.5,1.0,0);
   glTexCoord2d(0,0); glVertex3f(-1.0,0.0,0);
   glTexCoord2d(0,1); glVertex3f(-0.3,0.0,8);
   glTexCoord2d(1,1); glVertex3f(-0.1,0.1,8);
   glEnd();
   //  Undo transofrmations
    //  Switch off textures so it doesn't apply to the rest
   //glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw xwing main body
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 */
static void bodyM(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(dx,dy,dz);
    
   //Texture 
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[1]);
   glColor3f(1,1,1);
   //  Cube
   glBegin(GL_POLYGON);
   //  Back polygon
   glColor3f(1,1,1);
   glNormal3f(0.0,0.0,-1);
   glTexCoord2f(0.25,0); glVertex3f(-0.5,-1.0,-3);
   glTexCoord2f(0.75,0); glVertex3f(+0.5,-1.0,-3);
   glTexCoord2f(1,0.5);    glVertex3f(+1.0,0,-3);
   glTexCoord2f(0.75,1);   glVertex3f(+0.5,+1.0,-3);
   glTexCoord2f(0.25,1);   glVertex3f(-0.5,+1.0,-3);
   glTexCoord2f(0,0.5);    glVertex3f(-1.0,0.0,-3);
   glEnd();
   // Front polygon
   glBegin(GL_POLYGON);
   //glColor3f(0,0,1);
   glNormal3f(0.0,0.0,1);
   glTexCoord2f(0.25,0.5); glVertex3f(-0.5,-1.0,0);
   glTexCoord2f(0.75,0.5); glVertex3f(+0.5,-1.0,0);
   glTexCoord2f(1,0.5);    glVertex3f(+1.0,0,0);
   glTexCoord2f(0.75,1);   glVertex3f(+0.5,+1.0,0);
   glTexCoord2f(0.25,1);   glVertex3f(-0.5,+1.0,0);
   glTexCoord2f(0,0.5);    glVertex3f(-1.0,0.0,0);
   glEnd();
   glBindTexture(GL_TEXTURE_2D,texture[12]);
   //up
   glBegin(GL_QUADS);
   //glColor3f(0.55,0.55,0.55);
   glNormal3f(0.0,1.0,0.0);
   glTexCoord2f(0,0);      glVertex3f(-0.5,+1.0,-3);
   glTexCoord2f(1,0);      glVertex3f(+0.5,+1.0,-3);
   glTexCoord2f(1,1);      glVertex3f(+0.5,+1.0,0);
   glTexCoord2f(0,1);      glVertex3f(-0.5,+1.0,0);
   //side up right
   glNormal3f(0.5,1.0,0.0);
   glTexCoord2f(0,0);     glVertex3f(+0.5,+1.0,-3);
   glTexCoord2f(1,0);     glVertex3f(+1.0,0.0,-3);
   glTexCoord2f(1,1);     glVertex3f(+1.0,0.0,0);
   glTexCoord2f(0,1);     glVertex3f(+0.5,+1.0,0);
   //side down right
   glNormal3f(0.5,-1.0,0.0);
   glTexCoord2f(0,0);    glVertex3f(+1.0,0.0,-3);
   glTexCoord2f(1,0);    glVertex3f(+0.5,-1.0,-3);
   glTexCoord2f(1,1);    glVertex3f(+0.5,-1.0,0);
   glTexCoord2f(0,1);    glVertex3f(+1.0,0.0,0);
   //bottom
   glNormal3f(0.0,-1.0,0.0);
   glTexCoord2f(0,0);    glVertex3f(+0.5,-1.0,-3);
   glTexCoord2f(1,0);    glVertex3f(-0.5,-1.0,-3);
   glTexCoord2f(1,1);    glVertex3f(-0.5,-1.0,0);
   glTexCoord2f(0,1);    glVertex3f(+0.5,-1.0,0);
   //side down left
   glNormal3f(-0.5,-1.0,0.0);
   glTexCoord2f(0,0);    glVertex3f(-0.5,-1.0,-3);
   glTexCoord2f(1,0);    glVertex3f(-1.0,0.0,-3);
   glTexCoord2f(1,1);    glVertex3f(-1.0,0.0,0);
   glTexCoord2f(0,1);    glVertex3f(-0.5,-1.0,0);
   //side up left
   glNormal3f(-0.5,1.0,0.0);
   glTexCoord2f(0,0);    glVertex3f(-1.0,0.0,-3);
   glTexCoord2f(1,0);    glVertex3f(-0.5,1.0,-3);
   glTexCoord2f(1,1);    glVertex3f(-0.5,1.0,0);
   glTexCoord2f(0,1);    glVertex3f(-1.0,0.0,0);
   glEnd();
   //  Undo transofrmations
    //  Switch off textures so it doesn't apply to the rest
   glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw the upper part of the wing of xwing 
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the z axis
 */
static void engine(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{

   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(th,0,0,1);
   glScaled(dx,dy,dz);
    
   // Texture 
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[15]);
   
   glColor3f(1,1,1);
   //  Cube
   glBegin(GL_QUADS);
   //  Top
   //glColor3f(1,1,1);
   glNormal3f( 0,+1, 0);
   glTexCoord2f(0,0);   glVertex3f(0.0,0.0,0.0);
   glTexCoord2f(1,0);   glVertex3f(+1.5,0.0,0.0);
   glTexCoord2f(0,1);   glVertex3f(+1.5,0.0,-2.5);
   glTexCoord2f(1,1);   glVertex3f(0.0,0.0,-2.5);
      //  Bottom
   //glColor3f(1,1,1);
   glNormal3f( 0,-1, 0);
   glTexCoord2f(0,0);   glVertex3f(0.0,0.25,0.0);
   glTexCoord2f(1,0);   glVertex3f(+1.5,0.25,0.0);
   glTexCoord2f(0,1);    glVertex3f(+1.5,0.25,-2.5);
   glTexCoord2f(1,1);   glVertex3f(0.0,0.25,-2.5);
   //  Front
   glColor3f(0.4,0.4,0.4);
   glNormal3f( 0, 0, 1);
   glVertex3f(0.0,0.0,0.0);
   glVertex3f(+1.5,0.0,0.0);
   glVertex3f(+1.5,0.25,0.0);
   glVertex3f(0.0,0.25,0.0);
   //  Back
   glColor3f(0.4,0.4,0.4);
   glNormal3f( 0, 0,-1);
   glVertex3f(+1.5,0.0,-2.5);
   glVertex3f(0.0,0.0,-2.5);
   glVertex3f(0.0,0.25,-2.5);
   glVertex3f(+1.5,0.25,-2.5);
      //  Right
   glColor3f(0.4,0.4,0.4);
   glNormal3f(+1, 0, 0);
   glVertex3f(+1.5,0.0,0.0);
   glVertex3f(+1.5,0.0,-2.5);
   glVertex3f(+1.5,0.25,-2.5);
   glVertex3f(+1.5,0.25,0.0);
      //  Left
   glColor3f(0.4,0.4,0.4);;
   glNormal3f(-1, 0, 0);
   glVertex3f(0.0,0.0,0.0);
   glVertex3f(0.0,0.0,-2.5);
   glVertex3f(0.0,0.25,-2.5);
   glVertex3f(0.0,0.25,0.0);
   //  End
   glEnd();
   //  Undo transofrmations
   //  Switch off textures so it doesn't apply to the rest
   // glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw xwing wing
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the z axis
 */
static void wing(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(th,0,0,1);
   glScaled(dx,dy,dz);
    
   // Texture 
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[3]);
   glColor3f(1,1,1);
   
   //up 
   glBegin(GL_QUADS);
   glColor3f(0.5,0.5,0.5);
   glNormal3f(0.0,1.0,0.0);
   glTexCoord2f(0,0);  glVertex3f(0.0,0.05,0.0);
   glTexCoord2f(1,0);  glVertex3f(+4.0,0.05,0.0);
   glTexCoord2f(1,1);  glVertex3f(+4.0,0.05,-2.0);
   glTexCoord2f(0,1);  glVertex3f(0.0,0.05,-3.0);
   //down
   glNormal3f(0.0,-1.0,0.0);
   glTexCoord2f(0,0);  glVertex3f(0.0,0.0,0.0);
   glTexCoord2f(1,0);  glVertex3f(+4.0,0.0,0.0);
   glTexCoord2f(1,1);  glVertex3f(+4.0,0.0,-2.0);
   glTexCoord2f(0,1);  glVertex3f(0.0,0.0,-3.0);
   //side front
   glNormal3f(0.0,0.0,1.0);
   glTexCoord2f(0,0);  glVertex3f(0.0,0.0,0.0);
   glTexCoord2f(1,0);  glVertex3f(+4.0,0.0,0.0);
   glTexCoord2f(1,1);  glVertex3f(+4.0,0.05,0.0);
   glTexCoord2f(0,1);  glVertex3f(0.0,0.05,0.0);
   //side outside
   glNormal3f(1.0,0.0,0.0);
   glTexCoord2f(0,0);  glVertex3f(+4.0,0.0,0.0);
   glTexCoord2f(1,0);  glVertex3f(+4.0,0.0,-2.0);
   glTexCoord2f(1,1);  glVertex3f(+4.0,0.05,-2.0);
   glTexCoord2f(0,1);  glVertex3f(+4.0,0.05,0.0);
   //side back
   glNormal3f(0.0,0.0,-1.0);
   glTexCoord2f(0,0);  glVertex3f(+4.0,0.0,-2.0);
   glTexCoord2f(1,0);  glVertex3f(0.0,0.0,-3.0);
   glTexCoord2f(1,1);  glVertex3f(0.0,0.05,-3.0);
   glTexCoord2f(0,1);  glVertex3f(+4.0,0.05,-2.0);
   //side inside
   glNormal3f(-1.0,0.0,0.0);
   glTexCoord2f(0,0);  glVertex3f(0.0,0.0,-3.0);
   glTexCoord2f(1,0);  glVertex3f(0.0,0.0,0.0);
   glTexCoord2f(1,1);  glVertex3f(0.0,0.05,0.0);
   glTexCoord2f(0,1);  glVertex3f(0.0,0.05,-3.0);
   glEnd();

   if(th == 20 || th == -160 || th == 0.1|| th == 180){
      engine(0.0,0.01,0,1,0.9,1,0);
      cylinder(0.8,0.6,-1.5,0.4,2,90,0,0,texture[2]);
      cylinder(0.8,0.6,-3,0.2,2.5,90,0,0,texture[10]);
      cylinder(4.0,0.0,-2.3,0.1,3.0,90,0,0,texture[6]);
      cylinder(4.0,0.0,0.7,0.02,3.5,90,0,0,texture[2]);
   }else{
      engine(0.0,-0.01,0,1,-0.9,1,0);
      cylinder(0.7,-0.6,-1.5,0.4,2,90,0,0,texture[2]);
      cylinder(0.7,-0.6,-3,0.2,2.5,90,0,0,texture[10]);
      cylinder(3.9,0.0,-2.3,0.1,3.0,90,0,0,texture[6]);
      cylinder(3.9,0.0,0.7,0.02,3.5,90,0,0,texture[2]);
   }
   //  Undo transofrmations
   //  Switch off textures so it doesn't apply to the rest
   glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw xwing cockpit
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 */
static void cockpitW(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{

   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(dx,dy,dz);
    
   // Texture 
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[7]);
   glColor3f(1,1,1);
   //  Cube
   glBegin(GL_QUADS);
   //  Front
   //glColor3f(1,1,1);
   glNormal3f(0.0,1,1);
   glTexCoord2f(0.0,1); glVertex3f(0.0,0.5,0);
   glTexCoord2f(1.0,1); glVertex3f(1.0,0.5,0);
   glTexCoord2f(1.0,0.0); glVertex3f(1,0,1);
   glTexCoord2f(0.0,0.0); glVertex3f(0.0,0.0,1);
   //  End
   glEnd();
   //  Undo transofrmations
   //  Switch off textures so it doesn't apply to the rest
   glColor3f(1,1,1);
   //  Cube
   glBegin(GL_TRIANGLES);
   // right
   //glColor3f(1,1,1);
   glNormal3f( 1, 0, 0);
   glTexCoord2f(0.0,0.0); glVertex3f(1,0, 1);
   glTexCoord2f(1.0,0); glVertex3f(1,0,0);
   glTexCoord2f(1.0,1.0); glVertex3f(1,0.5, 0);
   glEnd();
   // left

   glBegin(GL_TRIANGLES);
   glNormal3f( -1, 0, 0);
   glTexCoord2f(0.0,0); glVertex3f(0,0,1);
   glTexCoord2f(1.0,0); glVertex3f(0,0,0);
   glTexCoord2f(1.0,1.0); glVertex3f(0,0.5,0);

   //  End
   glEnd();
   cubeN(0.5,0.20,-0.5,0.5,0.3,0.5,0);

   glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw xwing 
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 */
static void drawXwing(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th){
  
   glTranslated(mx,my,z);
   glRotated(th,0,1,0);
   glScaled(dx,dy,dz);
   
   if(mx <-15){ 
      mx = -15;
   }else if(mx > 15){ 
      mx =15;
   }
   if(my <-4){ 
      my = -4;
   }else if(my > 15){
      my =15;
   }
   bodyM(x,y,z,1,1,1,1);
   bodyF(x,y,z,1,1,1,1);
   nose(x+0.13,y,z+7.8,1,1,1,1);
   if(wingMode ==1){   
      wing(x+0.5,y+0.5,z+0,1,1,1,20);
      wing(x+0.5,y-0.5,z+0,1,1,1,-20);
      wing(x-0.5,y+0.5,z+0,1,1,1,160);
      wing(x-0.5,y-0.5,z+0,1,1,1,-160);
   }else{
      wing(x+1.0,y+0.1,z+0,1,1,1,0.1);
      wing(x+1.0,y-0.1,z+0,1,1,1,-0.1);
      wing(x-1.0,y+0.1,z+0,1,1,1,-180);
      wing(x-1.0,y-0.1,z+0,1,1,1,180);
   }
   moon(x+0.0,y+1.0,z-0.7,0.3,texture[0]);
   cockpitW(x-0.5,y+0.75,z+0.8,1,1,1,0);
}
/* 
 *  Draw sky box
 *  from ex25.c
 */
static void Space(double D)
{
   glColor3f(1,1,1);
   glEnable(GL_TEXTURE_2D);
   //  Sides
   glBindTexture(GL_TEXTURE_2D,texture[5]);
   glBegin(GL_QUADS);
   glTexCoord2f(0,0); glVertex3f(-D,-D,-D);
   glTexCoord2f(1,0); glVertex3f(+D,-D,-D);
   glTexCoord2f(1,1); glVertex3f(+D,+D,-D);
   glTexCoord2f(0,1); glVertex3f(-D,+D,-D);

   glTexCoord2f(1,0); glVertex3f(+D,-D,-D);
   glTexCoord2f(0,0); glVertex3f(+D,-D,+D);
   glTexCoord2f(0,1); glVertex3f(+D,+D,+D);
   glTexCoord2f(1,1); glVertex3f(+D,+D,-D);

   glTexCoord2f(0,0); glVertex3f(+D,-D,+D);
   glTexCoord2f(1,0); glVertex3f(-D,-D,+D);
   glTexCoord2f(1,1); glVertex3f(-D,+D,+D);
   glTexCoord2f(0,1); glVertex3f(+D,+D,+D);

   glTexCoord2f(1,0); glVertex3f(-D,-D,+D);
   glTexCoord2f(0,0); glVertex3f(-D,-D,-D);
   glTexCoord2f(0,1); glVertex3f(-D,+D,-D);
   glTexCoord2f(1,1); glVertex3f(-D,+D,+D);
   glEnd();

   //  Top and bottom
   glBindTexture(GL_TEXTURE_2D,texture[5]);
   glBegin(GL_QUADS);
   glTexCoord2f(0,0); glVertex3f(+D,+D,-D);
   glTexCoord2f(1,0); glVertex3f(+D,+D,+D);
   glTexCoord2f(0,1); glVertex3f(-D,+D,+D);
   glTexCoord2f(1,1); glVertex3f(-D,+D,-D);

   glTexCoord2f(0,1); glVertex3f(-D,-D,+D);
   glTexCoord2f(1,0); glVertex3f(+D,-D,+D);
   glTexCoord2f(0,1); glVertex3f(+D,-D,-D);
   glTexCoord2f(1,1); glVertex3f(-D,-D,-D);
   glEnd();

   glDisable(GL_TEXTURE_2D);
}

/**
 * drawBullet
 * Draws a bullet
 */
void drawBullet(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{
   //static double bOffset = 0.0; //scroll 
   glTranslatef(x,y,z);
   glRotated(th,0,0,1);
   glScaled(dx,dy,dz); 
   glTranslatef(0, 0, bOffset);

   //  Save transformation
   glPushMatrix();
   
   //  Offset, scale and rotate
    if(wingMode ==1){ 
      glColor3f(0.5, 0.0, 0.0);
      glLineWidth(7);
      glBegin(GL_LINES);
      glVertex3f(4.3,2,14.5);
      glVertex3f(4.3,2,16.5);
      glVertex3f(4.3,-2,14.5);
      glVertex3f(4.3,-2,16.5);
      glVertex3f(-4.3,2,14.5);
      glVertex3f(-4.3,2,16.5);
      glVertex3f(-4.3,-2,14.5);
      glVertex3f(-4.3,-2,16.5);
      glEnd();
    }else{
      glColor3f(0.5, 0.0, 0.0);
      glLineWidth(7);
      glBegin(GL_LINES);
      glVertex3f(4.3,0,14.5);
      glVertex3f(4.3,0,16.5);
      glVertex3f(4.3,0,14.5);
      glVertex3f(4.3,0,16.5);
      glVertex3f(-4.3,0,14.5);
      glVertex3f(-4.3,0,16.5);
      glVertex3f(-4.3,0,14.5);
      glVertex3f(-4.3,0,16.5);
      glEnd();
    }

   glPopMatrix();
}
/**
 * drawExplosion
 * Draws the explosion when you crash into the sea
 */
void drawExplosion(int slices, int stacks) 
{
	int i, j;
	for (i = 0; i <= slices; i++) {
		float lat0 = M_PI * (-0.5 + (float) (i - 1) / slices);
		float z0 = sin(lat0);
		float zr0 = cos(lat0);

		float lat1 = M_PI * (-0.5 + (float) i / slices);
		float z1 = sin(lat1);
		float zr1 = cos(lat1);

		glBegin(GL_QUAD_STRIP);
		for (j = 0; j <= stacks; j++) {
			float lng = 2 * M_PI * (float) (j - 1) / stacks;
			//randomize the position to create somewhat of a jagged shape .
			float x = cos(lng)
					+ (((randBetween(0, 1000) / 1000.0f) - 0.5f) / 10.0f);
			float y = sin(lng)
					+ (((randBetween(0, 1000) / 1000.0f) - 0.5f) / 10.0f);
			glNormal3f(x * zr0, y * zr0, z0);
			glVertex3f(x * zr0, y * zr0, z0);
			glNormal3f(x * zr1, y * zr1, z1);
			glVertex3f(x * zr1, y * zr1, z1);
		}
		glEnd();
	}
}
/**
 * explode
 * 
 */
void explode() {
	glEnable(GL_COLOR_MATERIAL);
	// set material properties which will be assigned by glColor
	glColorMaterial(GL_FRONT, GL_DIFFUSE);
	int i;
	for (i = 0; i < 10; i++) {
		explosionScale += delta / 50.0f;
		glScalef(explosionScale, explosionScale, explosionScale);
		glColor4f(1, 0, 0, 0.2f);
		glRotatef(randBetween(0, 360), 1, 0, 0);

		glDisable(GL_CULL_FACE);
		drawExplosion(32, 32);
		glEnable(GL_CULL_FACE);
      //move = 0;
	}
	if (explosionScale > 1) {
		explodeMode = 0;
		explosionScale = 1.0;
      alive = 0;
      if(plive1 == 1){
         plive1 = 0;
      }
      if(plive2 == 1){
         plive2 = 0;
      }
      
      glDisable(GL_CULL_FACE);
	}
	glDisable(GL_COLOR_MATERIAL);
}
/**
 * explode
 * 
 */
void Pexplode() {
	glEnable(GL_COLOR_MATERIAL);
	// set material properties which will be assigned by glColor
	glColorMaterial(GL_FRONT, GL_DIFFUSE);
	int i;
	for (i = 0; i < 10; i++) {
		explosionScale += delta / 50.0f;
		glScalef(explosionScale, explosionScale, explosionScale);
		glColor4f(1, 0, 0, 0.2f);
		glRotatef(randBetween(0, 360), 1, 0, 0);

		glDisable(GL_CULL_FACE);
		drawExplosion(32, 32);
		glEnable(GL_CULL_FACE);
      //move = 0;
	}
	if (explosionScale > 1) {
		PexplodeMode = 0;
		explosionScale = 1.0;
      //boolShoot = 0;
      if(plive1 == 1){
         plive1 = 0;
      }
      if(plive2 == 1){
         plive2 = 0;
      }
      
      glDisable(GL_CULL_FACE);
	}
	glDisable(GL_COLOR_MATERIAL);
}
/*
 * cockpit mode
 * 
 */
void cockpit(double x,double y,double z,
            double dx,double dy,double dz,
            double th, unsigned int texture)
{

   glEnable(GL_COLOR_MATERIAL);
   //  Screen edge
   //float w = asp>2 ? asp : 2;
   //  Save transform attributes (Matrix Mode and Enabled Modes)
   glPushAttrib(GL_TRANSFORM_BIT|GL_ENABLE_BIT);
   //  Save projection matrix and set unit transform
   glMatrixMode(GL_PROJECTION);
   glPushMatrix();
   glLoadIdentity();
   glOrtho(-asp,+asp,-1,1,-1,1);
   // //  Save model view matrix and set to indentity
   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();
   glLoadIdentity();
   //  Draw instrument panel with texture
   glColor3f(1,1,1);
   glEnable(GL_TEXTURE_2D);
   glBindTexture(GL_TEXTURE_2D, texture);
   //lower panel
   glBegin(GL_QUADS);
   glTexCoord2d(0.0,0.0);glVertex2f(-2,-1);
   glTexCoord2d(1.0,0.0);glVertex2f(2,-1);
   glTexCoord2d(1.0,0.40);glVertex2f(2,-0.3);
   glTexCoord2d(0.0,0.40);glVertex2f(-2,-0.3);
   glEnd();
   // upper panel
   glBegin(GL_QUADS);
   glTexCoord2d(0.1,0.4);glVertex2f(-1.6,-0.3);
   glTexCoord2d(0.9,0.4);glVertex2f(1.6,-0.3);
   glTexCoord2d(0.62,0.57);glVertex2f(0.15, 0);
   glTexCoord2d(0.48,0.6);glVertex2f(-0.15, 0);
   glEnd();
  
   glDisable(GL_TEXTURE_2D);
   // Draw the inside of the cockpit in grey
   glColor3f(0.6,0.6,0.6); 
   glBegin(GL_QUADS);
   //  windsiheld left
   glVertex2f(-3,2);
   glVertex2f(-2.9,2);
   glVertex2f(-0.13,-0.2);
   glVertex2f(-0.23,-0.2);
   //  windshield right
   glVertex2f(3,2);
   glVertex2f(2.9,2);
   glVertex2f(0.13,-0.2);
   glVertex2f(0.23,-0.2);
   glEnd();
   
   glPopMatrix();
   //  Reset projection matrix
   glMatrixMode(GL_PROJECTION);
   glPopMatrix();
   //  Pop transform attributes (Matrix Mode and Enabled Modes)
   glPopAttrib();
   glDisable(GL_COLOR_MATERIAL);
}
/*
 *  Draw Space down part
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 */
static void drawSpace(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th)
{
   double ii;
   glTranslated(x,y,z);
   glRotated(th,0,0,1);
   glScaled(dx,dy,dz);
   //  Save transformation
   glPushMatrix();
   // Texture
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , mode?GL_REPLACE:GL_MODULATE);
   glColor3f(1.0,1.0,1.0);
   glBindTexture(GL_TEXTURE_2D,texture[6]);
   glTranslatef(0.0f, 0.0f,zOffset);
      
   glBegin(GL_QUADS);
   glNormal3f(0,+1,0);
   ii = -32;
   for( ii = -32.0;ii<32.0;ii+=4.0){
      glTexCoord2f(0.0,0.0); glVertex3f(8,-4,ii);
      glTexCoord2f(1.0,0.0); glVertex3f(8,-4,ii-2);
      glTexCoord2f(1.0,1.0); glVertex3f(-8,-4,ii-2);
      glTexCoord2f(0.0,1.0); glVertex3f(-8,-4,ii);
   }
   glEnd();
   cube(0,-8,-32,30,2,15,0,1);
   cube(0,-8,0,30,2,15,0,1);
   // draw asteroid
   if(plive1 == 1){
      moon(px1,py1,-12,pr1,texture[8]);
   }else if(plive1 == 0){
      moon(-30,-30,-30,0,texture[8]);
      plive1 = 1;
   }
   if(plive2 == 1){
      moon(px2,py2,-20,pr2,texture[13]);
   }else if(plive2 == 0){
      moon(-30,-30,-30,0,texture[13] );
      plive2 = 1;
   }

   if(PexplodeMode == 1) {
      Pexplode();
   }
   
   glPopMatrix();
   glDisable(GL_TEXTURE_2D);
   ErrCheck("Space");
}

/*
 *  OpenGL (GLUT) calls this routine to display the scene
 */
void display()
{

   const double len=2.0;  //  Length of axes
   //appX = glutGet((GLenum) GLUT_WINDOW_X);
	appY = glutGet((GLenum) GLUT_WINDOW_Y);

   //  Erase the window and the depth buffer
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
   //  Enable Z-buffering in OpenGL
   glEnable(GL_DEPTH_TEST);
   //glEnable(GL_CULL_FACE);
   //  Undo previous transformations
   glLoadIdentity();
   //  Perspective - set eye position

   if (light)
   {
      //  Translate intensity to color vectors
      float Ambient[]   = {0.01*ambient ,0.01*ambient ,0.01*ambient ,1.0};
      float Diffuse[]   = {0.01*diffuse ,0.01*diffuse ,0.01*diffuse ,1.0};
      float Specular[]  = {0.01*specular,0.01*specular,0.01*specular,1.0};
      //  Light position
      float Position[]  = {distance*Cos(zh),ylight,distance*Sin(zh),1.0};
      //  Draw light position as ball (still no lighting here)
      glColor3f(1,1,1);
      ball(Position[0],Position[1],Position[2] , 0.1);
      //  OpenGL should normalize normal vectors
      glEnable(GL_NORMALIZE);
      //  Enable lighting
      glEnable(GL_LIGHTING);
      //  Location of viewer for specular calculations
      glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,local);
      //  glColor sets ambient and diffuse color materials
      glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
      glEnable(GL_COLOR_MATERIAL);
      //  Enable light 0
      glEnable(GL_LIGHT0);
      //  Set ambient, diffuse, specular components and position of light 0
      glLightfv(GL_LIGHT0,GL_AMBIENT ,Ambient);
      glLightfv(GL_LIGHT0,GL_DIFFUSE ,Diffuse);
      glLightfv(GL_LIGHT0,GL_SPECULAR,Specular);
      glLightfv(GL_LIGHT0,GL_POSITION,Position);
   }
   else
      glDisable(GL_LIGHTING);
if(cockpitMode == 0){

   if (mode)
    {
        double Ex = -2*dim*Sin(th)*Cos(ph);
        double Ey = +2*dim        *Sin(ph);
        double Ez = +2*dim*Cos(th)*Cos(ph);
        gluLookAt(Ex,Ey,Ez , 0,0,0 , 0,Cos(ph),0);
    }
    //  Orthogonal - set world orientation
    else
    {
        glRotatef(ph,1,0,0);
        glRotatef(th,0,1,0);
    }
   //  Draw sky
   Space(3*dim);

   drawSpace(0,-1,0,1,1,1,0);
   // Flat or smooth shading
   // glShadeModel(smooth ? GL_SMOOTH : GL_FLAT);
   //  Light switch
   if(alive == 1 ){
      drawXwing(0,0,5,0.3,0.3,0.3,180);
   }

   if(boolShoot == 1){
         drawBullet(0,0,0,1,1,1,0);
   }

   if(explodeMode == 1) {
		explode();
	}
}else if(cockpitMode == 1){
   glTranslated(-mx,-my,-5);
   glRotated(th,0,1,0);
  
   if(mx <-15){ 
      mx = -15;
   }else if(mx > 15){ 
      mx =15;
   }
   if(my <-4){ 
      my = -4;
   }else if(my > 15){
      my =15;
   }

   //  Draw sky
   Space(3*dim);

   drawSpace(0,-1,0,1,1,1,0);
   // Flat or smooth shading
   // glShadeModel(smooth ? GL_SMOOTH : GL_FLAT);
   
   if(alive == 1 ){
      cockpit(0,0,0,1,1,1,0,texture[9]);
   }
   if(boolShoot == 1){
      drawBullet(0,0,0,1,1,1,0);
   }
   if(explodeMode == 1) {
		explode();
	}
}else{
   
   if (mode)
    {
        double Ex = -2*dim*Sin(th)*Cos(ph);
        double Ey = +2*dim        *Sin(ph);
        double Ez = +2*dim*Cos(th)*Cos(ph);
        gluLookAt(Ex,Ey,Ez , 0,0,0 , 0,Cos(ph),0);
    }
    //  Orthogonal - set world orientation
    else
    {
        glRotatef(ph,1,0,0);
        glRotatef(th,0,1,0);
    }
   
   //glRotated(zh,0,1,0);
   drawXwing(0,0,0,0.5,0.5,0.5,-40);
}
   //  Draw axes
   glColor3f(1,1,1);
   if (axes)
   {
      glBegin(GL_LINES);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(len,0.0,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,len,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,0.0,len);
      glEnd();
      //  Label axes
      glRasterPos3d(len,0.0,0.0);
      Print("X");
      glRasterPos3d(0.0,len,0.0);
      Print("Y");
      glRasterPos3d(0.0,0.0,len);
      Print("Z");
   }
   //  Display parameters

   glColor3f(0,1,0);
   glWindowPos2i(5,65);
   Print("Mode change:c , game mode =>cockpit mode=> X-wing, wing shape change: q, lazer shoot: z, random asteroid: j, Restart: r");
   glWindowPos2i(5,5);
   Print("Angle=%d,%d  Dim=%.1f FOV=%d Projection=%s Light=%s",th,ph,dim,fov,mode?"Perpective":"Orthogonal",light?"On":"Off");
   if (light)
   {
      glWindowPos2i(5,45);
      Print("x = %.1f y = %.1f Speed = %.1f ",mx,my,thrust);
      Print("Model=%s LocalViewer=%s Distance=%d Elevation=%.1f",smooth?"Smooth":"Flat",local?"On":"Off",distance,ylight);
      glWindowPos2i(5,25);
      Print("Ambient=%d  Diffuse=%d Specular=%d Emission=%d Shininess=%.0f",ambient,diffuse,specular,emission,shiny);
   }
   //  Render the scene and make it visible
   ErrCheck("display");
   glFlush();
   glutSwapBuffers();
}

/*
 *  GLUT calls this routine when the window is resized
 */
void idle()
{
   //  Elapsed time in seconds
   double t = glutGet(GLUT_ELAPSED_TIME)/1000.0;
   zh = fmod(90*t,360.0);
   //printf("\nzh: %d",zh);
   // moving under
   if(zOffset > 24.0){
      zOffset = -24.0;
      if(randomMode == 1){
         px1 = randBetween(-5,5);
         py1 = randBetween(-5,5);
         px2 = randBetween(-5,5);
         py2 = randBetween(-5,5);
      }else{
         px1 = 0.0;
         py1 = 1.0;
         px2 = 1.0;
         py2 = 2.0;
      }
   }
   zOffset += thrust;
   if(cockpitMode == 1){
      if(boolShoot == 0){
         bOffset = -5;
      }else if(bOffset > 24.0){
         bOffset = -5.0;
      }else{
         bOffset -= thrust;
      }

   }else{
      if(boolShoot == 0){
         bOffset = -5;
      }else if(bOffset > 24.0){
         bOffset = -5.0;
      }else{
         bOffset += thrust;
      }
   }

// xwing collision
   if( (px1-pr1) < mx  && (px1+pr1) > mx && (py1-pr1) < my  && (py1+0.2) > my && zOffset>13.2)
   {
      explodeMode++;
   }
   if( (px2-pr2) < mx  && (px2+pr2) > mx && (py2-pr2) < my  && (py2+pr2) > my && zOffset > 22)
   {
      explodeMode++;
   }

   if(boolShoot == 1 && zOffset > 0.0){
      for(int i = 0; i<2 ; i++)
      {
         for(int j = 0; j<2 ; j++)
         {
            if((px1-pr1) < mx+wx[i]  && (px1+pr1) > mx+wx[i] && (py1-pr1) < my+wy[j]  && (py1+pr1) > my+wy[j] && (abs(zOffset-12) - bOffset)<1.0)
            {
               PexplodeMode++;
            }
         }
      }

   }
   
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when an arrow key is pressed
 */
void special(int key,int x,int y)
{
   //  Right arrow key - increase angle by 5 degrees
   if (key == GLUT_KEY_RIGHT)
      th += 5;
   //  Left arrow key - decrease angle by 5 degrees
   else if (key == GLUT_KEY_LEFT)
      th -= 5;
   //  Up arrow key - increase elevation by 5 degrees
   else if (key == GLUT_KEY_UP)
      ph += 5;
   //  Down arrow key - decrease elevation by 5 degrees
   else if (key == GLUT_KEY_DOWN)
      ph -= 5;
   //  PageUp key - increase dim
   else if (key == GLUT_KEY_PAGE_UP)
      dim += 0.1;
   //  PageDown key - decrease dim
   else if (key == GLUT_KEY_PAGE_DOWN && dim>1)
      dim -= 0.1;
   //  Smooth color model
   else if (key == GLUT_KEY_F1)
      smooth = 1-smooth;
   //  Local Viewer
   else if (key == GLUT_KEY_F2)
      local = 1-local;
   else if (key == GLUT_KEY_F3)
      distance = (distance==1) ? 5 : 1;
   //  Toggle ball increment
   else if (key == GLUT_KEY_F8)
      inc = (inc==10)?3:10;
   //  Flip sign
   else if (key == GLUT_KEY_F9)
      one = -one;
   //  Keep angles to +/-360 degrees
   th %= 360;
   ph %= 360;
  //  Update projection
   Project(mode?fov:0,asp,dim);
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when a key is pressed
 */
void key(unsigned char ch,int x,int y)
{
   //  Exit on ESC
   if (ch == 27)
      exit(0);
   //  Reset view angle
   else if (ch == '0')
      th = ph = mx = my= 0;
   //  Toggle axes
   else if (ch == 'x' || ch == 'X')
      axes = 1-axes;
   //  Toggle lighting
   else if (ch == 'l' || ch == 'L')
      light = 1-light;
   //  Switch projection mode
   else if (ch == 'p' || ch == 'P')
      mode = 1-mode;
   //  Toggle light movement
   else if (ch == 'm' || ch == 'M')
      move = 1-move;
   // Toggle wing
   else if (ch == 'q' || ch == 'Q')
      wingMode = 1-wingMode;
   //  Move right
   else if (ch == 'a' || ch == 'a')
      mx -= 0.2;
   //  Move left
   else if (ch == 'd' || ch == 'D')
      mx += 0.2;
   //  move up
   else if (ch == 'w' || ch == 'W')
      my += 0.2;
   //  move down
   else if (ch == 's' || ch == 'S')
      my -= 0.2;
   // lazer shoot
   else if (ch == 'z' || ch == 'Z')
      boolShoot = 1-boolShoot;
   else if (ch == 'g' || ch == 'G'){
      if (thrust > 1)
         thrust = 0.2;
      else if(thrust <0.2)
         thrust = 0.2;
      thrust = thrust - 0.1;
   }
   else if (ch == 'h' || ch == 'H'){
      if (thrust > 1)
         thrust = 0.2;
      else if(thrust < 0.2)
         thrust = 0.2;
      thrust = thrust + 0.1;
   }
   // restart
   else if (ch == 'r' || ch == 'R'){
      alive = 1;
      mx = 0.0;
      my = 0.0;
   }
   // random asteroid
   else if (ch == 'j' || ch == 'J')
      randomMode = 1-randomMode;
   // cockpit modechange
   else if (ch == 'c' || ch == 'C'){
      cockpitMode++;
      cockpitMode = cockpitMode%3;
   }
   //  Move light
   else if (ch == '<')
      zh += 1;
   else if (ch == '>')
      zh -= 1;     
   //  Change field of view angle
   else if (ch == '-' && ch>1)
      fov--;
   else if (ch == '+' && ch<179)
      fov++;
   //  Light elevation
   else if (ch=='[')
      ylight -= 0.1;
   else if (ch==']')
      ylight += 0.1;
   //  Ambient level
   else if (ch=='t' && ambient>0)
      ambient -= 5;
   else if (ch=='T' && ambient<100)
      ambient += 5;
   //  Diffuse level
   else if (ch=='y' && diffuse>0)
      diffuse -= 5;
   else if (ch=='Y' && diffuse<100)
      diffuse += 5;
   //  Specular level
   else if (ch=='u' && specular>0)
      specular -= 5;
   else if (ch=='U' && specular<100)
      specular += 5;
   //  Emission level
   else if (ch=='i' && emission>0)
      emission -= 5;
   else if (ch=='I' && emission<100)
      emission += 5;
   //  Shininess level
   else if (ch=='o' && shininess>-1)
      shininess -= 1;
   else if (ch=='O' && shininess<7)
      shininess += 1;
   //  Translate shininess power to value (-1 => 0)
   shiny = shininess<0 ? 0 : pow(2.0,shininess);
   //  Reproject
   Project(mode?fov:0,asp,dim);
   //  Animate if requested
   glutIdleFunc(move?idle:NULL);
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}
/*
 * key Down
 */
void keyDown(unsigned char key, int mouseX, int mouseY) {
	//z for shooting
	if (key == 'z') {
		boolShoot = 1;
	}
}
/*
 *  GLUT calls this routine when the window is resized
 */
void reshape(int width,int height)
{
   //  Ratio of the width to the height of the window
   asp = (height>0) ? (double)width/height : 1;
   //  Set the viewport to the entire window
   glViewport(0,0, width,height);
   //  Set projection
   Project(mode?fov:0,asp,dim);
}

/*
 *  Start up GLUT and tell it what to do
 */
int main(int argc,char* argv[])
{
   //  Initialize GLUT
   glutInit(&argc,argv);
   //  Request double buffered, true color window with Z buffering at 800x600
   glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
   glutInitWindowSize(1200,700);
   glutCreateWindow("Final Project :Jaeyoung Oh");
   //  Set callbacks
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutSpecialFunc(special);
   glutKeyboardFunc(key);
   //glutKeyboardUpFunc(keyUp);
   //glutKeyboardFunc(keyDown);
   //Load texture
   texture[0] = LoadTexBMP("R2.bmp");
   texture[1] = LoadTexBMP("engine.bmp");
   texture[2] = LoadTexBMP("cannon_rod2.bmp");
   texture[3] = LoadTexBMP("wing_3a.bmp");
   texture[4] = LoadTexBMP("solar.bmp");
   texture[5] = LoadTexBMP("milk.bmp");
   texture[6] = LoadTexBMP("corn.bmp");
   texture[7] = LoadTexBMP("canopy.bmp");
   texture[8] = LoadTexBMP("moon.bmp");
   texture[9] = LoadTexBMP("cockpit.bmp");
   texture[10] = LoadTexBMP("cannonBack.bmp");
   texture[11] = LoadTexBMP("nose_all.bmp");
   texture[12] = LoadTexBMP("nose_up.bmp");
   texture[13] = LoadTexBMP("asteroid.bmp");
   texture[14] = LoadTexBMP("bodyF.bmp");
   texture[15] = LoadTexBMP("engineUP.bmp");
   
   
   glutIdleFunc(idle);
   //  Pass control to GLUT so it can interact with the user
   ErrCheck("init");
   glutMainLoop();
   return 0;
}
