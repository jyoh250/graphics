/*
 *  HW2 : Lorenzo Attractor
 *  Name: Jaeyung Oh
 *  SID : 108601183
 *
 *  Key bindings:
 *  s/d   change s
 *  b/n   change b 
 *  r/t   change r
 *  9      Reset s,b,r 
 *  arrows Change view angle
 *  0      Reset view angle
 *  ESC    Exit
 *  For HW2, I modified ex7.c and lorenz.c provided by Prof. Schreuder for this course.
 *  Time : 6hours
 *   - ex7.c and lorenz.c code review: 3H
 *   - HW2.c coding: 3H
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
//  OpenGL with prototypes for glext
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

int th=0;         //  Azimuth of view angle
int ph=0;         //  Elevation of view angle
int axes=1;       //  Display axes
int mode=0;       //  Depth mode
int rev=0;        //  Reverse bottom of cube
int n=1;          //  Number of cubes

/*  Lorenz Parameters  */
#define NO_IT 50000  // No of iteration for Lorenzo
double s  = 10;      // default value
double b  = 2.6666;  // default value
double r  = 28;      // default value

/*
 *  Convenience routine to output raster text
 *  Use VARARGS to make this more flexible
 */
#define LEN 8192  //  Maximum length of text string
void Print(const char* format , ...)
{
   char    buf[LEN];
   char*   ch=buf;
   va_list args;
   //  Turn the parameters into a character string
   va_start(args,format);
   vsnprintf(buf,LEN,format,args);
   va_end(args);
   //  Display the characters one at a time at the current raster position
   while (*ch)
      glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,*ch++);
}

/*
 *  Lorenzo function
 */
void lorenz()
{
   double x = 1;
   double y = 1;
   double z = 1;
   double dx = 0;
   double dy = 0;
   double dz = 0;
   int i = 0;
   int n = NO_IT;
   //time difference
   double dt = 0.001;

   for (i = 0; i<n; i++)
   {
     dx = s*(y-x);
     dy = x*(r-z)-y;
     dz = x*y - b*z;
     x += dx*dt;
     y += dy*dt;
     z += dz*dt;
     glColor3f(x/10,y/20,z/30);  // color change
     glVertex3f(x/30,y/30,z/30); // change the vertex values to fit the axes in this program
   }
}

/*
 *  OpenGL (GLUT) calls this routine to display the scene
 */
void display()
{
   const double len=1.5;  //  Length of axes
   //  Erase the window and the depth buffer
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

   //  Undo previous transformations
   glLoadIdentity();
   //  Set view angle
   glRotatef(ph,1,0,0);
   glRotatef(th,0,1,0);

   //  Draw a lorenz 
   glBegin(GL_LINE_STRIP);
   lorenz();
   glEnd();
   
   //  White
   glColor3f(1,1,1);
   //  Draw axes
   glBegin(GL_LINES);
   glVertex3d(0.0,0.0,0.0);
   glVertex3d(len,0.0,0.0);
   glVertex3d(0.0,0.0,0.0);
   glVertex3d(0.0,len,0.0);
   glVertex3d(0.0,0.0,0.0);
   glVertex3d(0.0,0.0,len);
   glEnd();
    //  Label axes
   glRasterPos3d(len,0.0,0.0);
   Print("X");
   glRasterPos3d(0.0,len,0.0);
   Print("Y");
   glRasterPos3d(0.0,0.0,len);
   Print("Z");
   
   //  Five pixels from the lower left corner of the window
   glWindowPos2i(5,30);
   //  Print the text string
   Print("Angle=%d,%d" ,th,ph);
   Print(" s=%.3f,b =%.3f, r =%.3f",s,b,r);
   //  Render the scene
   glFlush();
   //  Make the rendered scene visible
   glutSwapBuffers();
}

/*
 *  GLUT calls this routine when an arrow key is pressed
 */
void special(int key,int x,int y)
{
   //  Right arrow key - increase angle by 5 degrees
   if (key == GLUT_KEY_RIGHT)
      th += 5;
   //  Left arrow key - decrease angle by 5 degrees
   else if (key == GLUT_KEY_LEFT)
      th -= 5;
   //  Up arrow key - increase elevation by 5 degrees
   else if (key == GLUT_KEY_UP)
      ph += 5;
   //  Down arrow key - decrease elevation by 5 degrees
   else if (key == GLUT_KEY_DOWN)
      ph -= 5;
   //  Keep angles to +/-360 degrees
   th %= 360;
   ph %= 360;
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when a key is pressed
 */
void key(unsigned char ch,int x,int y)
{
   //  Exit on ESC
   if (ch == 27)
      exit(0);
   //  Reset view angle
   else if (ch == '0')
      th = ph = 0;
   // Reset s,b,r
   else if (ch == '9')
   {
      s = 10;
      b = 2.6666;
      r = 28;
   }
   //  change s 
   else if (ch == 'd' || ch == 'D')
      s = s + 0.1;
   else if (ch == 's' || ch == 'S')
      s = s - 0.1;
   //  change b
   else if (ch == 'n' || ch == 'N')
      b = b + 0.1;
   else if (ch == 'b' || ch == 'B')
      b = b - 0.1;
   //  chnage r 
   else if (ch == 't' || ch == 'T')
      r = r + 0.1;
   else if (ch == 'r' || ch == 'R')
      r = r - 0.1;
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when the window is resized
 */
void reshape(int width,int height)
{
   const double dim=2.5;
   //  Ratio of the width to the height of the window
   double w2h = (height>0) ? (double)width/height : 1;
   //  Set the viewport to the entire window
   glViewport(0,0, width,height);
   //  Tell OpenGL we want to manipulate the projection matrix
   glMatrixMode(GL_PROJECTION);
   //  Undo previous transformations
   glLoadIdentity();
   //  Orthogonal projection
   glOrtho(-w2h*dim,+w2h*dim, -dim,+dim, -dim,+dim);
   //  Switch to manipulating the model matrix
   glMatrixMode(GL_MODELVIEW);
   //  Undo previous transformations
   glLoadIdentity();
}

/*
 *  Start up GLUT and tell it what to do
 */
int main(int argc,char* argv[])
{
   //  Initialize GLUT and process user parameters
   glutInit(&argc,argv);
   //  Request double buffered, true color window with Z buffering
   glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
   //  Request 500 x 500 pixel window
   glutInitWindowSize(500,500);
   //  Create the window
   glutCreateWindow("HW2:Jaeyoung Oh");
   //  Tell GLUT to call "display" when the scene should be drawn
   glutDisplayFunc(display);
   //  Tell GLUT to call "reshape" when the window is resized
   glutReshapeFunc(reshape);
   //  Tell GLUT to call "special" when an arrow key is pressed
   glutSpecialFunc(special);
   //  Tell GLUT to call "key" when a key is pressed
   glutKeyboardFunc(key);
   //  Pass control to GLUT so it can interact with the user
   glutMainLoop();
   return 0;
}

