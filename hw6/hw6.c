/*
 * Homework 6 : Texture
 * Prof. Willem A. (Vlakkies) Schreuder
 * Course: CSCI4229/5229 Fall 2018
 * Name: Jaeyoung Oh
 * SID : 108601183
 *
 *  Key bindings:
 *  l        Toggles lighting
 *  a/A        Decrease/increase ambient light
 *  d/D        Decrease/increase diffuse light
 *  s/S        Decrease/increase specular light
 *  e/E        Decrease/increase emitted light
 *  n/N        Decrease/increase shininess
 *  F1         Toggle smooth/flat shading
 *  F2         Toggle local viewer mode
 *  F3         Toggle light distance (1/5)
 *  F8         Change ball increment
 *  F9         Invert bottom normal
 *  m          Toggles light movement
 *  []         Lower/rise light
 *  p          Toggles ortogonal/perspective projection
 *  +/-        Change field of view of perspective
 *  x          Toggle axes
 *  arrows     Change view angle
 *  PgDn/PgUp  Zoom in and out
 *  0          Reset view angle
 *  ESC        Exit
 * 
 *   Arrows    Change view angle
 *	 Right: Increase azimuth by 5 degrees
 *	 Left : Decrease azimuth by 5 degrees
 *	 Up   : Increase elevation by 5 degrees
 *   Down : Decrease elevation by 5 degrees
 * 
 * I modified my hw5.c and ex13.c ,ex14.c exmaple provided by Prof. Schreuder for this course.
 * 
 * Time: total 15 hours
 * - web search about the texture and example 14 code review: 5H
 * - HW6xc.c coding: 10H
 */


#include "CSCIx229.h"

int axes=1;       //  Display axes
int mode=1;       //  Projection mode
int move=1;       //  Movie light
int th=0;         //  Azimuth of view angle
int ph=0;         //  Elevation of view angle
int fov=55;       //  Field of view (for perspective)
double asp=1;     //  Aspect ratio
double dim=5.0;   //  Size of world

// Light values
int light     =   1;  //lighting
int one       =   1;  // Unit value
int distance  =   5;  // Light distance
int inc       =  10;  // Ball increment
int smooth    =   1;  // Smooth/Flat shading
int local     =   0;  // Local Viewer Model
int emission  =   0;  // Emission intensity (%)
int ambient   =  30;  // Ambient intensity (%)
int diffuse   = 100;  // Diffuse intensity (%)
int specular  =   0;  // Specular intensity (%)
int shininess =   0;  // Shininess (power of two)
float shiny   =   1;  // Shininess (value)
int zh        =  90;  // Light azimuth
float ylight  =   0;  // Elevation of light
unsigned int texture[8];  //  Texture names
int ntx = 0;           // Texture number
float rep = 0.0;      // cube texture repeat number

/*
 *  Draw vertex in polar coordinates with normal from ex13.c add texture coordinate
 */
static void Vertex(double th,double ph)
{
   double x = Sin(th)*Cos(ph);
   double y = Cos(th)*Cos(ph);
   double z =         Sin(ph);
   //  For a sphere at the origin, the position
   //  and normal vectors are the same
   glNormal3d(x,y,z);
   glTexCoord2d(th/360.0,ph/180.0+0.5);
   glVertex3d(x,y,z);
}

/*
 *  Draw a cube from ex13.c and add texture functioin
 *     at (x,y,z)
 *     dimensions (dx,dy,dz)
 *     rotated th about the y axis
 *     rep: y repeat number of texture
 */
static void cube(double x,double y,double z,
                 double dx,double dy,double dz,
                 double th, float rep)
{
   //  Set specular color to white
   float white[] = {1,1,1,1};
   float black[] = {0,0,0,1};
   glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,white);
   glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,black);
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(th,0,1,0);
   glScaled(dx,dy,dz);
    
   // Texture 
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[0]);
   glColor3f(1,1,1);
   //  Cube
   glBegin(GL_QUADS);
   //  Front
   //glColor3f(1,1,1);
   glNormal3f( 0, 0, 1);
   glTexCoord2f(0.0,rep); glVertex3f(-1,-1, 1);
   glTexCoord2f(1.0,rep); glVertex3f(+1,-1, 1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,+1, 1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,+1, 1);
   //  Back
   //glColor3f(1,1,1);
   glNormal3f( 0, 0,-1);
   glTexCoord2f(0.0,rep); glVertex3f(+1,-1,-1);
   glTexCoord2f(1.0,rep); glVertex3f(-1,-1,-1);
   glTexCoord2f(1.0,0.0); glVertex3f(-1,+1,-1);
   glTexCoord2f(0.0,0.0); glVertex3f(+1,+1,-1);
      //  Right
   //glColor3f(1,1,1);
   glNormal3f(+1, 0, 0);
   glTexCoord2f(0.0,rep); glVertex3f(+1,-1,+1);
   glTexCoord2f(1.0,rep); glVertex3f(+1,-1,-1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,+1,-1);
   glTexCoord2f(0.0,0.0); glVertex3f(+1,+1,+1);
      //  Left
   //glColor3f(1,1,1);
   glNormal3f(-1, 0, 0);
   glTexCoord2f(0.0,rep); glVertex3f(-1,-1,-1);
   glTexCoord2f(1.0,rep); glVertex3f(-1,-1,+1);
   glTexCoord2f(1.0,0.0); glVertex3f(-1,+1,+1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,+1,-1);
      //  Top
   //glColor3f(1,1,1);
   glNormal3f( 0,+1, 0);
   glTexCoord2f(0.0,rep); glVertex3f(-1,+1,+1);
   glTexCoord2f(1.0,rep); glVertex3f(+1,+1,+1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,+1,-1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,+1,-1);
      //  Bottom
   //glColor3f(1,1,1);
   glNormal3f( 0,-1, 0);
   glTexCoord2f(0.0,rep); glVertex3f(-1,-1,-1);
   glTexCoord2f(1.0,rep); glVertex3f(+1,-1,-1);
   glTexCoord2f(1.0,0.0); glVertex3f(+1,-1,+1);
   glTexCoord2f(0.0,0.0); glVertex3f(-1,-1,+1);
     //  End
   glEnd();
   //  Undo transofrmations
    //  Switch off textures so it doesn't apply to the rest
   glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}

/*
 *  Draw a ball from ex13.c
 *     at (x,y,z)
 *     radius (r)
 */
static void ball(double x,double y,double z,double r)
{
   int th,ph;
   float yellow[] = {1.0,1.0,0.0,1.0};
   float Emission[]  = {0.0,0.0,0.01*emission,1.0};
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glScaled(r,r,r);

   //  White ball
   glColor3f(1,1,1);
   glMaterialf(GL_FRONT,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT,GL_SPECULAR,yellow);
   glMaterialfv(GL_FRONT,GL_EMISSION,Emission);
   //  Bands of latitude
   for (ph=-90;ph<90;ph+=inc)
   {
      glBegin(GL_QUAD_STRIP);
      for (th=0;th<=360;th+=2*inc)
      {
         Vertex(th,ph);
         Vertex(th,ph+inc);
      }
      glEnd();
   }
   //  Undo transofrmations
   glPopMatrix();
}
/*
 *  Draw a moon from ex18.c
 *     at (x,y,z)
 *     radius (r)
 */
static void moon(double x,double y,double z,double r)
{
   int th,ph;
   float yellow[] = {1.0,1.0,0.0,1.0};
   float Emission[]  = {0.0,0.0,0.01*emission,1.0};
   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glScaled(r,r,r);
   /*
    *  Draw surface of the planet
    */
   // Set texture
   glEnable(GL_TEXTURE_2D);
   glBindTexture(GL_TEXTURE_2D,texture[7]);

   //  White ball
   glColor3f(1,1,1);
   glMaterialf(GL_FRONT,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT,GL_SPECULAR,yellow);
   glMaterialfv(GL_FRONT,GL_EMISSION,Emission);
   //  Bands of latitude
   for (ph=-90;ph<90;ph+=inc)
   {
      glBegin(GL_QUAD_STRIP);
      for (th=0;th<=360;th+=2*inc)
      {
         Vertex(th,ph);
         Vertex(th,ph+inc);
      }
      glEnd();
   }
   //  Undo transofrmations
   glDisable(GL_TEXTURE_2D);
   glPopMatrix();
}
/*
 *  Draw a cone
 *     at (x,y,z)
 *     size (r(radius), h(height))
 *     rotate (rx, ry, rz)
 */
void cone(double x,double y,double z,
          double r,double h, 
          double rx, double ry, double rz)
{
   int inc=10;
   int th;

   //  Set specular color to white
   float white[] = {1,1,1,1};
   float black[] = {0,0,0,1};
   glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,white);
   glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,black);

   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(rx,1,0,0);
   glRotated(ry,0,1,0);
   glRotated(rz,0,0,1);
   glScaled(r,h,r);

   glColor3f(0.8,0.8,0.8);
   // Upper Part
   glBegin(GL_TRIANGLE_FAN);
   glVertex3f(0,1,0);
   for (th=0;th<=360;th+=inc)
   {
      glNormal3f(Sin(th),0,Cos(th));
      glVertex3d(Sin(th),0,Cos(th));
   }
   glEnd();

   // Base Part
   glBegin(GL_TRIANGLE_FAN);
   glVertex3f(0,0,0);
   glNormal3f(0,-1,0);
   for (th=0;th<=360;th+=2*inc)
   {
      glVertex3d(Sin(th),0,Cos(th));
   }
   glEnd();

   //  Undo transformations and textures
   glPopMatrix();
}
/*
 *  Draw a cone with texture
 *     at (x,y,z)
 *     size (r(radius), h(height))
 *     rotate (rx, ry, rz)
 */
void coneTex(double x,double y,double z,
          double r,double h, 
          double rx, double ry, double rz)
{
   int inc=10;
   int th;

   //  Set specular color to white
   float white[] = {1,1,1,1};
   float black[] = {0,0,0,1};
   glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,white);
   glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,black);

   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(rx,1,0,0);
   glRotated(ry,0,1,0);
   glRotated(rz,0,0,1);
   glScaled(r,h,r);

   //  Enable textures
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
   glColor3f(1.0, 1.0, 1.0); //Set color to white
   glBindTexture(GL_TEXTURE_2D,texture[6]);

   // Upper Part
   glBegin(GL_TRIANGLE_FAN);
   glTexCoord2f(0.5,0.5); glVertex3f(0,1,0);
   for (th=0;th<=360;th+=inc)
   {
      glNormal3f(Sin(th),0,Cos(th));
      glTexCoord2f(2*Sin(th)+0.5,2); glVertex3d(Sin(th),0,Cos(th));
   }
   glEnd();

   // Base Part
   glBegin(GL_TRIANGLE_FAN);
   glVertex3f(0,0,0);
   glNormal3f(0,-1,0);
   for (th=0;th<=360;th+=2*inc)
   {
      glVertex3d(Sin(th),0,Cos(th));
   }
   glEnd();
   
   glDisable(GL_TEXTURE_2D);
   //  Undo transformations and textures
   glPopMatrix();
}
/*
 *  Draw a cylinder with texture 
 *     at (x,y,z)
 *     size (r(radius), h(height))
 *     rotate (rx, ry, rz)
 */
void cylinder(double x,double y,double z,
              double r,double h, 
              double rx, double ry, double rz)
{
   int inc=5;
   int th;

   //  Set specular color to white
   float white[] = {1,1,1,1};
   float black[] = {0,0,0,1};
   glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,white);
   glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,black);

   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(rx,1,0,0);
   glRotated(ry,0,1,0);
   glRotated(rz,0,0,1);
   glScaled(r,h,r);

   //  Enable textures
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[1]);

   glColor3f(1,1,1);
   // Upper Part
   glBegin(GL_TRIANGLE_FAN);
   glTexCoord2f(0.5,0.5); 
   glVertex3f(0,1,0);
   glNormal3f(0,1,0);
   for (th=0;th<=360;th+=2*inc)
   {
      glTexCoord2f(2*Sin(th)+0.5,2*Cos(th)+0.5);
      glVertex3d(Sin(th),1,Cos(th));
   }
   glEnd();

   // Side Part
   glBegin(GL_QUAD_STRIP); 
   for (th=0;th<=360;th+=inc)
   {
      double u = th/360.0;
      glNormal3f(Cos(th),0,Sin(th));
      glTexCoord2d(10*u,4.0); glVertex3d(Cos(th),1,Sin(th));
      glTexCoord2d(10*u,0.0); glVertex3d(Cos(th),0,Sin(th));
    }
   glEnd();

   // Base Part
   glBegin(GL_TRIANGLE_FAN);
   glTexCoord2f(0.5,0.5); 
   glVertex3f(0,0,0);
   glNormal3f(0,-1,0);
   for (th=0;th<=360;th+=2*inc)
   {
      glTexCoord2f(2*Sin(th)+0.5,2*Cos(th)+0.5);
      glVertex3d(Sin(th),0,Cos(th));
   }
   glEnd();

   //  Undo transformations and textures
   glPopMatrix();
   glDisable(GL_TEXTURE_2D);
}
/*
 *  Draw a cylinder with texture
 *     at (x,y,z)
 *     size (r(radius), h(height))
 *     rotate (rx, ry, rz)
 */
void cylinderT(double x,double y,double z,
              double r,double h, 
              double rx, double ry, double rz)
{
   int inc=5;
   int th;

   //  Set specular color to white
   float white[] = {1,1,1,1};
   float black[] = {0,0,0,1};
   glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,white);
   glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,black);

   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(rx,1,0,0);
   glRotated(ry,0,1,0);
   glRotated(rz,0,0,1);
   glScaled(r,h,r);

   //  Enable textures
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[4]);

   glColor3f(1,1,1);
   // Upper Part
   glBegin(GL_TRIANGLE_FAN);
   glTexCoord2f(0.5,0.5); 
   glVertex3f(0,1,0);
   glNormal3f(0,1,0);
   for (th=0;th<=360;th+=2*inc)
   {
      glTexCoord2f(2*Sin(th)+0.5,2*Cos(th)+0.5);
      glVertex3d(Sin(th),1,Cos(th));
   }
   glEnd();

   // Side Part
   glBegin(GL_QUAD_STRIP); 
   for (th=0;th<=360;th+=inc)
   {
      double u = th/360.0;
      glNormal3f(Cos(th),0,Sin(th));
      glTexCoord2d(4*u,1.0); glVertex3d(Cos(th),1,Sin(th));
      glTexCoord2d(4*u,0.0); glVertex3d(Cos(th),0,Sin(th));
    }
   glEnd();

   // Base Part
   glBegin(GL_TRIANGLE_FAN);
   glTexCoord2f(0.5,0.5); 
   glVertex3f(0,0,0);
   glNormal3f(0,-1,0);
   for (th=0;th<=360;th+=2*inc)
   {
      glTexCoord2f(2*Sin(th)+0.5,2*Cos(th)+0.5);
      glVertex3d(Sin(th),0,Cos(th));
   }
   glEnd();

   //  Undo transformations and textures
   glPopMatrix();
   glDisable(GL_TEXTURE_2D);
}
/*
 *  Draw a Sign board
 *     at (x,y,z)
 *     size (r(radius), h(height))
 *     rotate (rx, ry, rz)
 */
void cylinderT2(double x,double y,double z,
              double r,double h, 
              double rx, double ry, double rz,
              int ntx)
{
   int inc=5;
   int th;

   //  Set specular color to white
   float white[] = {1,1,1,1};
   float black[] = {0,0,0,1};
   glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,white);
   glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,black);

   //  Save transformation
   glPushMatrix();
   //  Offset, scale and rotate
   glTranslated(x,y,z);
   glRotated(rx,1,0,0);
   glRotated(ry,0,1,0);
   glRotated(rz,0,0,1);
   glScaled(r,h,r);

   //  Enable textures
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D,texture[ntx]);
  
   // Side Part
   glBegin(GL_QUAD_STRIP); 
   for (th=0;th<=100;th+=inc)
   {
      //double u = th/120.0;
      glNormal3f(Cos(th),0,Sin(th));
      glTexCoord2d(Cos(th), 0.0); glVertex3d(Cos(th),0,Sin(th));
      glTexCoord2d(Cos(th), 1.0); glVertex3d(Cos(th),1,Sin(th));

    }
   glEnd();

   //  Undo transformations and textures
   glPopMatrix();
   glDisable(GL_TEXTURE_2D);
}
/*
 *  OpenGL (GLUT) calls this routine to display the scene
 */
void display()
{
   int i,k;
   double j;
   const double len=2.0;  //  Length of axes
   //  Erase the window and the depth buffer
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
   //  Enable Z-buffering in OpenGL
   glEnable(GL_DEPTH_TEST);
   //  Undo previous transformations
   glLoadIdentity();
   
   if (mode)
    {
        double Ex = -2*dim*Sin(th)*Cos(ph);
        double Ey = +2*dim        *Sin(ph);
        double Ez = +2*dim*Cos(th)*Cos(ph);
        gluLookAt(Ex,Ey,Ez , 0,0,0 , 0,Cos(ph),0);
    }
    //  Orthogonal - set world orientation
    else
    {
        glRotatef(ph,1,0,0);
        glRotatef(th,0,1,0);
    }

   //  Flat or smooth shading
   glShadeModel(smooth ? GL_SMOOTH : GL_FLAT);

 //  Light switch
   if (light)
   {
      //  Translate intensity to color vectors
      float Ambient[]   = {0.01*ambient ,0.01*ambient ,0.01*ambient ,1.0};
      float Diffuse[]   = {0.01*diffuse ,0.01*diffuse ,0.01*diffuse ,1.0};
      float Specular[]  = {0.01*specular,0.01*specular,0.01*specular,1.0};
      //  Light position
      float Position[]  = {distance*Cos(zh),ylight,distance*Sin(zh),1.0};
      //  Draw light position as ball (still no lighting here)
      glColor3f(1,1,1);
      ball(Position[0],Position[1],Position[2] , 0.1);
      //  OpenGL should normalize normal vectors
      glEnable(GL_NORMALIZE);
      //  Enable lighting
      glEnable(GL_LIGHTING);
      //  Location of viewer for specular calculations
      glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,local);
      //  glColor sets ambient and diffuse color materials
      glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
      glEnable(GL_COLOR_MATERIAL);
      //  Enable light 0
      glEnable(GL_LIGHT0);
      //  Set ambient, diffuse, specular components and position of light 0
      glLightfv(GL_LIGHT0,GL_AMBIENT ,Ambient);
      glLightfv(GL_LIGHT0,GL_DIFFUSE ,Diffuse);
      glLightfv(GL_LIGHT0,GL_SPECULAR,Specular);
      glLightfv(GL_LIGHT0,GL_POSITION,Position);
   }
   else
      glDisable(GL_LIGHTING);

   // Draw scene
   // The ground
   float white[] = {1,1,1,1};
   float black[] = {0,0,0,1};
   
   glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,shiny);
   glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,white);
   glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,black);
   
   //  Save transformation
   glPushMatrix();
   
   // Texture
   glEnable(GL_TEXTURE_2D);
   glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE , mode?GL_REPLACE:GL_MODULATE);
   glColor3f(1.0,1.0,1.0);
   glBindTexture(GL_TEXTURE_2D,texture[3]);
      
   glBegin(GL_QUADS);
   glNormal3f(0,+1,0);
   glTexCoord2f(0.0,0.0); glVertex3f(2,-3.5,2);
   glTexCoord2f(1.0,0.0); glVertex3f(2,-3.5,-2);
   glTexCoord2f(1.0,1.0); glVertex3f(-2,-3.5,-2);
   glTexCoord2f(0.0,1.0); glVertex3f(-2,-3.5,2);
   glEnd();
   glPopMatrix();
   glDisable(GL_TEXTURE_2D);

   // Draw space rocket
   for(i=0;i<2;i++){
       for(j=0;j<2;j++){
            cylinder((0.3+(i*-0.6)),-3.25,(0.3+(j*-0.6)),0.2,0.3,0,0,0);
            coneTex((0.3+(i*-0.6)),-3.5,(0.3+(j*-0.6)),0.3,1,0,0,0);
       }
   }
   // body
   for(j=0;j<2;j++){
        cylinder(0,-3+(j*3),0,(0.6-(j/10)),3-j,0,0,0);
        cylinderT(0,-2.5+(j*3),0,(0.61-(j/10)),0.5,0,0,0);
   }
   cylinder(0,2,0,0.4,1,0,0,0);
   cylinderT2(0,2.2,0,0.41,0.3,0,0,0,5);
   cone(0,3,0,0.4,1.3,0,0,0);
   
   //support block 1
   cube(-1,-0.5,0.1,0.05,3,0.05,0,34);
   cube(-1.2,-0.5,0.1,0.05,3,0.05,0,34);
   cube(-1.2,-0.5,-0.1,0.05,3,0.05,0,34);
   cube(-1,-0.5,-0.1,0.05,3,0.05,0,34);
   //support block 2
   j = -3.3;
   for(i = 0;i<4;i++){
        cube(-1.1,j,0,0.2,0.2,0.2,180,1);
        if(i>=1){
            for(k = 0;k<5;k++){
                cube(-0.8+(1*0.1),j,0.1,0.5,0.05,0.05,0,1);
                cube(-0.8+(1*0.1),j,-0.1,0.5,0.05,0.05,0,1);
            }
        }
        j = j+2;
   }

   moon(1,3,-2,0.3);
   glDisable(GL_LIGHTING);
   //  Draw axes
   glColor3f(1,1,1);
   if (axes)
   {
      glBegin(GL_LINES);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(len,0.0,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,len,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,0.0,len);
      glEnd();
      //  Label axes
      glRasterPos3d(len,0.0,0.0);
      Print("X");
      glRasterPos3d(0.0,len,0.0);
      Print("Y");
      glRasterPos3d(0.0,0.0,len);
      Print("Z");
   }
   //  Display parameters
   glWindowPos2i(5,5);
   
   Print("Angle=%d,%d  Dim=%.1f FOV=%d Projection=%s Light=%s",
     th,ph,dim,fov,mode?"Perpective":"Orthogonal",light?"On":"Off");
   if (light)
   {
      glWindowPos2i(5,45);
      Print("Model=%s LocalViewer=%s Distance=%d Elevation=%.1f",smooth?"Smooth":"Flat",local?"On":"Off",distance,ylight);
      glWindowPos2i(5,25);
      Print("Ambient=%d  Diffuse=%d Specular=%d Emission=%d Shininess=%.0f",ambient,diffuse,specular,emission,shiny);
   }
   //  Render the scene and make it visible
   ErrCheck("display");
   glFlush();
   glutSwapBuffers();
}

/*
 *  GLUT calls this routine when the window is resized
 */
void idle()
{
   //  Elapsed time in seconds
   double t = glutGet(GLUT_ELAPSED_TIME)/1000.0;
   zh = fmod(90*t,360.0);
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when an arrow key is pressed
 */
void special(int key,int x,int y)
{
   //  Right arrow key - increase angle by 5 degrees
   if (key == GLUT_KEY_RIGHT)
      th += 5;
   //  Left arrow key - decrease angle by 5 degrees
   else if (key == GLUT_KEY_LEFT)
      th -= 5;
   //  Up arrow key - increase elevation by 5 degrees
   else if (key == GLUT_KEY_UP)
      ph += 5;
   //  Down arrow key - decrease elevation by 5 degrees
   else if (key == GLUT_KEY_DOWN)
      ph -= 5;
   //  PageUp key - increase dim
   else if (key == GLUT_KEY_PAGE_UP)
      dim += 0.1;
   //  PageDown key - decrease dim
   else if (key == GLUT_KEY_PAGE_DOWN && dim>1)
      dim -= 0.1;
   //  Smooth color model
   else if (key == GLUT_KEY_F1)
      smooth = 1-smooth;
   //  Local Viewer
   else if (key == GLUT_KEY_F2)
      local = 1-local;
   else if (key == GLUT_KEY_F3)
      distance = (distance==1) ? 5 : 1;
   //  Toggle ball increment
   else if (key == GLUT_KEY_F8)
      inc = (inc==10)?3:10;
   //  Flip sign
   else if (key == GLUT_KEY_F9)
      one = -one;
   //  Keep angles to +/-360 degrees
   th %= 360;
   ph %= 360;
  //  Update projection
   Project(mode?fov:0,asp,dim);
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when a key is pressed
 */
void key(unsigned char ch,int x,int y)
{
   //  Exit on ESC
   if (ch == 27)
      exit(0);
   //  Reset view angle
   else if (ch == '0')
      th = ph = 0;
   //  Toggle axes
   else if (ch == 'x' || ch == 'X')
      axes = 1-axes;
   //  Toggle lighting
   else if (ch == 'l' || ch == 'L')
      light = 1-light;
   //  Switch projection mode
   else if (ch == 'p' || ch == 'P')
      mode = 1-mode;
   //  Toggle light movement
   else if (ch == 'm' || ch == 'M')
      move = 1-move;
   //  Move light
   else if (ch == '<')
      zh += 1;
   else if (ch == '>')
      zh -= 1;     
   //  Change field of view angle
   else if (ch == '-' && ch>1)
      fov--;
   else if (ch == '+' && ch<179)
      fov++;
   //  Light elevation
   else if (ch=='[')
      ylight -= 0.1;
   else if (ch==']')
      ylight += 0.1;
   //  Ambient level
   else if (ch=='a' && ambient>0)
      ambient -= 5;
   else if (ch=='A' && ambient<100)
      ambient += 5;
   //  Diffuse level
   else if (ch=='d' && diffuse>0)
      diffuse -= 5;
   else if (ch=='D' && diffuse<100)
      diffuse += 5;
   //  Specular level
   else if (ch=='s' && specular>0)
      specular -= 5;
   else if (ch=='S' && specular<100)
      specular += 5;
   //  Emission level
   else if (ch=='e' && emission>0)
      emission -= 5;
   else if (ch=='E' && emission<100)
      emission += 5;
   //  Shininess level
   else if (ch=='n' && shininess>-1)
      shininess -= 1;
   else if (ch=='N' && shininess<7)
      shininess += 1;
 
   //  Translate shininess power to value (-1 => 0)
   shiny = shininess<0 ? 0 : pow(2.0,shininess);
   //  Reproject
   Project(mode?fov:0,asp,dim);
   //  Animate if requested
   glutIdleFunc(move?idle:NULL);
   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();
}

/*
 *  GLUT calls this routine when the window is resized
 */
void reshape(int width,int height)
{
   //  Ratio of the width to the height of the window
   asp = (height>0) ? (double)width/height : 1;
   //  Set the viewport to the entire window
   glViewport(0,0, width,height);
   //  Set projection
   Project(mode?fov:0,asp,dim);
}

/*
 *  Start up GLUT and tell it what to do
 */
int main(int argc,char* argv[])
{
   //  Initialize GLUT
   glutInit(&argc,argv);
   //  Request double buffered, true color window with Z buffering at 800x600
   glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
   glutInitWindowSize(800,900);
   glutCreateWindow("HW6:Jaeyoung Oh");
   //  Set callbacks
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutSpecialFunc(special);
   glutKeyboardFunc(key);
   //  Load texture
   texture[0] = LoadTexBMP("crate.bmp");
   texture[1] = LoadTexBMP("wall2.bmp");
   texture[2] = LoadTexBMP("surface1.bmp");
   texture[3] = LoadTexBMP("grass.bmp");
   texture[4] = LoadTexBMP("solar.bmp");
   texture[5] = LoadTexBMP("space.bmp");
   texture[6] = LoadTexBMP("corn.bmp");
   texture[7] = LoadTexBMP("moon.bmp");
   glutIdleFunc(idle);
   //  Pass control to GLUT so it can interact with the user
   ErrCheck("init");
   glutMainLoop();
   return 0;
}
